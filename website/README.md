# Prerequisite

We use Bower as a dependency manager.
To install Bower you'll need to install NodeJS : 
```
http://nodejs.org/download/
```
Then open a terminal on run : 
```
npm install -g bower 
```

Finally go to the root of your project and run : 
```
bower install 
```
And you're good to go !

## Warning 
On some network there is firewall that can block bower, in that case run : 
```
git config --global url."https://github".insteadOf git://github
```
to use https instead .