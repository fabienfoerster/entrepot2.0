(function() {

var affichageDev = angular.module('affichageDev', ['sessionService', 'ngRoute']);

affichageDev.controller('ListeDevoirsEleve', ['$scope','$http','$timeout','Session',function($scope,$http,$timeout,Session) {
  $http.get('bouchons/devoirs_eleves.json').success(function(data){
    var res = new Array();
      $scope.nbPending = 0;
    data.forEach(function(element) {
          if (element.name == Session.getLogin()) {
              element.devoirs.forEach(function(e) {
                res.push(e);
                  if(e.state == "pending"){ $scope.nbPending++; }
            });
          }
    });
    $scope.devoirs = res;
    setCountdown();
  });

  var setCountdown = function() {
    for(var i = 0 ; i < $scope.devoirs.length; i++){
      $scope.devoirs[i].remainingTime = countdown($scope.devoirs[i].date_rendu) ;
    }
    $timeout(setCountdown , 1000);
  }

var countdown = function(date) {
    return moment().countdown(date).toString();
  };

  $scope.tab = "pending" ;
  $scope.setTab = function(selectedTab) {
    $scope.tab = selectedTab ;
  };

  $scope.isSet = function(selectedTab) {
    return $scope.tab === selectedTab ;
  };

}]);

})();
