var creationDevoir = angular.module('creationDevoir', ['sessionService']);
var idxDevGrp;

/** ---------- Enseignant - Creation d'un devoir ---------- **/

/* Liste des matieres */
creationDevoir.controller('CreerDevoirCtrl', ['$scope','$http', '$location', '$compile', 'Session', function($scope,$http,$location,$compile,Session,Devoir) {
      //Enable cross domain calls
      $http.defaults.useXDomain = true;
      //Remove the header used to identify ajax call  that would prevent CORS from working
      delete $http.defaults.headers.common['X-Requested-With'];
    
    var login = Session.getLogin();
    var matiereSelectionnee = "";
    
    $http.get('http://localhost:8080/jerseySpringJPA/devoir/matEns/'+login).success(function(data){
        var enseignants = data;
        $scope.matieres = new Array();
        enseignants.forEach(function(element) {
            $scope.matieres.push(element.name);
        });
    });
    
    idxDevGrp = 0;
    $scope.submitted = false;
    
    // mock des types dans lesquels rechercher
    $scope.typeslist = [];
    $http.get('js/extensions.json').success(function(data){
        $scope.typeslist = data;
    });
    
    // Recuperation
    $scope.recup = {};
    $scope.submit = function(form, newdev) {
        var devoir = {}; // Objet envoye au service web        
        $scope.submitted = true; // Formulaire soumis a validation
        if (form.$valid) {
            // Le formulaire est valide
            $scope.recup = angular.copy(newdev); // Recuperation des informations du formulaire
            devoir.matiere = newdev.mat;
            devoir.name = newdev.name;
            devoir.description = newdev.descr;
            devoir.enseignant = Session.getLogin();
            
            // Groupes
            devoir.checked = newdev.checked;
            if(newdev.checked == "true"){
                // Devoir par groupe
                devoir.groupes = [];
                if($scope.dispValid(newdev["gr"], "GrNewDev")){ 
                    // Le select de groupe courant est valide
                    var grp = {"name" : newdev.gr, "date" : newdev.date, "heure" : newdev.heure};
                    devoir.groupes.push(grp);
                }
                else{
                    return;
                }
                // Parcours des div de gestion de devoir par groupe
                for(var i = 0; i < idxDevGrp; i++){
                    if($("#devGr"+i).attr("deleted") == "false"){         
                        if(!$scope.dispValid(newdev["gr"+i], "GrNewDev"+i)){ return; }
                        if(!$scope.dispValid(newdev["date"+i], "DateNewDev"+i)){ return; }
                        if(!$scope.dispValid(newdev["heure"+i], "TimeNewDev"+i)){ return; }
                        var grp = {"name" : newdev["gr"+i], "date" : newdev["date"+i], "heure" : newdev["heure"+i]}; 
                        devoir.groupes.push(grp);       
                    }
                }
            } else {
                devoir.date = newdev.date;
                devoir.heure = newdev.heure;
            }
            
            // Extensions
            devoir.extensions = [];
            if($scope.newdev.selected.length > 0){
                for(var i = 0; i < $scope.newdev.selected.length; i++){
                    var ext = $scope.newdev.selected[i].name;
                    devoir.extensions.push(ext);
                }
            }
            
            var devStr = JSON.stringify(devoir);
            console.log(devStr);
            
            $http.post('http://localhost:8080/jerseySpringJPA/devoir/create', devoir).success(function(data, status, headers, config){
                $location.path("/");
            });
        }
    };
    
    $scope.dispValid = function(attr, elem){
        if(!attr){
            $("#err"+elem).css("display", "block");
            return false;
        }
        else{ 
            $("#err"+elem).css("display", "none"); 
            $("#ok"+elem).css("display", "block");
            return true;
        }
    }
    
    $scope.removeDevGr = function(nbItem) {
        var item = "#devGr" + nbItem;
        $(item).attr("deleted", true);
        $(item).remove();
    };
    
    $scope.checked = false;
    $scope.check = function(check) {
        $scope.checked = check;
        if(check == true){
            $("#selectGrp").attr("required",true);
        }
    };
    
    // On recupere la matiere selectionnee a chaque changement
    $scope.recupMat = function(mat) {
        matiereSelectionnee = mat;
        // Recuperation de la bonne liste de groupes (en fonction de la matiere selectionnee precedemment
        $http.get('http://localhost:8080/jerseySpringJPA/devoir/grpMat/'+mat).
        success(function(data){
            var listeGr = data;
            $scope.groupes = new Array();
            listeGr.forEach(function(element) {
                $scope.groupes.push(element.gr);
            })
        }).
        error(function(){
           $scope.groupes = []; 
        }); 
    }
    
    $scope.newdev = {};
    $scope.newdev.checked = false;
    $scope.newdev.selected = [];
    
}]);

creationDevoir.directive('addButton', function($compile){
    var linkFn = function(scope, element, attrs){        
        var addDiv = function(e){
            e.preventDefault();
            
            $("#addDev").append(
                $compile("<div class=\"panel\" id=\"devGr"+idxDevGrp+"\" deleted='false'><div class=\"panel-body\">"
                +"<ul class=\"nav nav-pills pull-right\">"
                    +"<li><button type=\"button\" class=\"btn btn-xs btn-primary\" ng-click=\"removeDevGr("+idxDevGrp+")\"><span class=\"glyphicon glyphicon-remove\"></span></button></li>"
                +"</ul>"
                +"<div class=\"form-group\">"
                    +"<label class=\"col-md-4 control-label\">Groupe</label>"
                    +"<div class=\"col-md-7 has-feedback\"><select class=\"form-control\" name=\"groupe\" ng-model=\"newdev.gr"+idxDevGrp+"\" ng-change=\"dispValid('newdev.gr"+idxDevGrp+"', 'GrNewDev"+idxDevGrp+"')\" ng-options=\"g for g in groupes\"></select>"
                    +"<span id=\"okGrNewDev"+idxDevGrp+"\" class=\"glyphicon glyphicon-ok form-control-feedback\" style=\"color:#3c763d;display:none;margin-right:1.5em;\"></span>"
                    +"<span id=\"errGrNewDev"+idxDevGrp+"\" class=\"glyphicon glyphicon-remove form-control-feedback\" style=\"color:#a94442;display:none;margin-right:1.5em;\"></span></div>"
                +"</div>"
                +"<div class=\"form-group\">"
                    +"<label class=\"col-md-4 control-label\" >Date*</label>"
                    +"<div class=\"col-md-5 has-feedback\">"
                        +"<input name=\"dateNewDev"+idxDevGrp+"\" class=\"form-control\" type=\"date\" ng-model=\"newdev.date"+idxDevGrp+"\" ng-change=\"dispValid('newdev.date"+idxDevGrp+"', 'DateNewDev"+idxDevGrp+"')\" required></input>"
                        +"<span id=\"okDateNewDev"+idxDevGrp+"\" class=\"glyphicon glyphicon-ok form-control-feedback\" style=\"color:#3c763d;display:none;\"></span>"
                        +"<span id=\"errDateNewDev"+idxDevGrp+"\" class=\"glyphicon glyphicon-remove form-control-feedback\" style=\"color:#a94442;display:none;\"></span>"
                    +"</div>"
                +"</div>"
                +"<div class=\"form-group\">"
                    +"<label class=\"col-md-4 control-label\" >Heure*</label>"
                    +"<div class=\"col-md-5 has-feedback\">"
                        +"<input name=\"timeNewDev"+idxDevGrp+"\" class=\"form-control\" type=\"time\" ng-model=\"newdev.heure"+idxDevGrp+"\" ng-change=\"dispValid('newdev.heure"+idxDevGrp+"', 'TimeNewDev"+idxDevGrp+"')\" required></input>"
                        +"<span id=\"okTimeNewDev"+idxDevGrp+"\" class=\"glyphicon glyphicon-ok form-control-feedback\" style=\"color:#3c763d;display:none;\"></span>"
                        +"<span id=\"errTimeNewDev"+idxDevGrp+"\" class=\"glyphicon glyphicon-remove form-control-feedback\" style=\"color:#a94442;display:none;\"></span>"
                    +"</div>"
            +"</div></div>")(scope)
            );
            
            scope.$apply();
            idxDevGrp++;
        };
        
        $('#addNewDevGr').on('click', addDiv);
    };
        
    return {
        restrict: 'E',
        template: "<div class='btn btn-primary' id='addNewDevGr' role='button'><span class='glyphicon glyphicon-plus'></span>Ajouter un rendu pour un autre groupe</div>",
        link: linkFn
    };
});

/* Autocompletion choix extensions */
creationDevoir.controller('TypeController', ['$scope',  function($scope) {
 
    // Tri de la liste de types
    $scope.typeslist.sort(function (a, b) {
        if (a.name > b.name)
          return 1;
        if (a.name < b.name)
          return -1;
        // a doit être égale à b
        return 0;
    });
 
    // saisie du nom du type
    $scope.type = null;
    
    $scope.addType = function(typeName) {
        if(typeName != null && typeName != ""){
            var i = 0;
            for(i = 0; i < $scope.typeslist.length; i++){
                if($scope.typeslist[i].name == typeName){ break; }
            }
            if(i == $scope.typeslist.length) { $scope.newdev.selected.push({"name": typeName}); }
            else { $scope.newdev.selected.push($scope.typeslist[i]); }

            $scope.typeslist.splice(i, 1);
            $scope.type = null;
        }
    };
    
    $scope.deleteType = function(typeName) {
        var i = 0;
        for(i = 0; i < $scope.newdev.selected.length; i++){
            if($scope.newdev.selected[i].name == typeName){ break; }
        }
        
        $scope.typeslist.push($scope.newdev.selected[i]);
        
        $scope.newdev.selected.splice(i, 1);
    }
    
}]);
