(function() {

    var modif = angular.module('modif', []);

    /** ---------- Enseignant - Recuperation d'un rendu ---------- **/
    modif.controller('ModifDevoirCtrl', ['$scope','$http','$routeParams', 'Session', function($scope, $http,$routeParams, Session) {
        $scope.renduID = $routeParams.renduID;

        $scope.devoir = {};
        $http.get('bouchons/'+$scope.renduID+'.json').success(function(data){
            $scope.devoir = data;
            
            $scope.newdev.mat = $scope.devoir.matiere;
            $scope.newdev.name = $scope.devoir.name;
            $scope.newdev.date = $scope.devoir.date;
            $scope.newdev.heure = $scope.devoir.heure;
            $scope.newdev.gr = $scope.devoir.groupes[0].name;
            $scope.newdev.checked = $scope.devoir.checked;

/*            for(var i = 1; i < $scope.devoir.groupes.length; i++){
                var name = $scope.devoir.groupes[i].name;
                $scope.newdev["gr"+idxDevGrp] = $scope.devoir.groupes[i].name;
                $scope.newdev["date"+idxDevGrp] = $scope.devoir.groupes[i].date;
                $scope.newdev["heure"+idxDevGrp] = $scope.devoir.groupes[i].heure;
                $('#addNewDevGr').trigger('click');
            }*/

            $scope.newdev.selected = $scope.devoir.files;
            $scope.newdev.selected.forEach(function(item){
                for(var i = 0; i < $scope.typeslist.length; i++){
                    if($scope.typeslist[i].name == item.name){ break; }
                }
                $scope.typeslist.splice(i, 1);
            });
            
            $scope.recupMat($scope.newdev.mat);
        });
    }]);

})();
