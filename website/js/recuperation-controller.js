(function() {


var recup = angular.module('recup', []);

/** ---------- Enseignant - Recuperation d'un rendu ---------- **/
recup.controller('EntrepotListEtudiantsCtrl', ['$scope','$http','$routeParams',function($scope, $http,$routeParams) {
    $scope.etudiants = new Array();
    $scope.renduID = $routeParams.renduID;
    
    // Decommenter pour recuperer le devoir depuis le service
    /*$http.get('http://localhost:8080/jerseySpringJPA/devoir/recupere/$scope.renduID').success(function(data){
        console.log(data);
        $scope.devoir = data;
    });*/
    
   $http.get('bouchons/'+$scope.renduID+'.json').success(function(data){
        $scope.devoir = data;
        
        // Liste des etudiants
        $scope.etudiants = $scope.devoir.rendu_etudiants;
        
        $scope.nbStudents = $scope.etudiants.length;
        
        // Calcule les proportions de rendus
        $scope.getProportion = function(state) {
            var nbState = 0;
            $scope.etudiants.forEach(function(element) {
                if(element.state == state){ nbState++; }
            });
            return nbState;
        };
        $scope.nbSuccess = $scope.getProportion('success');
        $scope.nbWarning = $scope.getProportion('warning');
        $scope.nbDanger = $scope.getProportion('danger');
        
        // Ajoute l'attribut 'checked' a chaque etudiant
        $scope.etudiants.forEach(function(element) {
            element.checked = false;
        });
    });

    /* Definition du filtre sur l'etat du rendu */
    $scope.query = '';
    // fonction de filtrage
    $scope.setQuery = function(state) {
        $scope.query = state;
        $scope.etudiants.forEach(function(element) {
            element.checked = false;
        });
        $scope.allChecked = false;
    };

    /* Definition des selections */
    $scope.allChecked = false;
    // Recupere la selection
    $scope.getSelection = function(){
        var selection = new Array();
        $scope.etudiants.forEach(function(element) {
            if(element.checked == true){
                selection.push(element);
            }
        });
        return selection;
    };

    // Affiche la selection
    $scope.displaySelection = function(){
        var selected = "Selection : \n";
        var selection = $scope.getSelection();
        selection.forEach(function(element) {
            selected += element.name + "\n";
        });
        alert(selected);
    };

    // Fonction du checkBox selectAll
    $scope.selectAll = function(){
        $scope.etudiants.forEach(function(element) {
            if($scope.query == element.state || $scope.query == ""){
                // selectAll + filtre
                element.checked = !$scope.allChecked;
            }
            else{
                element.checked = false;
            }
        });
        // Mise a jour du mailto
        $scope.mailSelection();
    };

    // Bouton mailto
    $scope.mailtoList = "";
    $scope.mailSelection = function(){
        var mailto = "mailto:";
        $scope.getSelection().forEach(function(element) {
            mailto += element.mail + ";";
        });
        $scope.mailtoList = (mailto == "mailto:") ? "#" : mailto;
    };
}]);

})();
