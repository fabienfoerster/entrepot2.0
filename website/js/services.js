(function() {

    var entrepotApp = angular.module("sessionService", ['ngCookies']);

    entrepotApp.factory('Session' ,['$window','$cookieStore', function(win,$cookieStore) {
      var login = $cookieStore.get('login') ;
      var status = $cookieStore.get('status') ;
        var logged = login != null ;

      this.setCredentials = function(log,stat){
          login = log ;
          status = stat;
          $cookieStore.put('login',login);
          $cookieStore.put('status',status);
          logged = true;

      };

      this.resetCredentials = function() {
          login = '';
          status = '';
          $cookieStore.remove('login');
          $cookieStore.remove('status');
          logged = false;
      }

      this.getLogin = function() {
        return login;
      };

      this.getStatus = function() {
        return status;
      };

        this.isLogged = function() {
            return logged;
        };


      return this;

    }]);


})();
