(function() {

  var app = angular.module('loginModule',['sessionService']);

  app.controller('loginCtrl',['$scope','$window','Session',function($scope,$window,Session) {
    $scope.login = '';
    $scope.status = '';

    $scope.connection = function(){
      Session.setCredentials($scope.login,$scope.status);
      if(Session.getStatus() == "etudiant"){
        $window.location.href = '#/entrepot/affichageEleve' ;
      } else if(Session.getStatus() == "enseignant"){
        $window.location.href = '#/entrepot/affichageEnseignant' ;
      } else {
        $window.location.href = "#/entrepot/"
      }
        $scope.$emit('loggedIn');
    };

  }]);




})();
