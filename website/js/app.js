(function(){
  
  var entrepotApp = angular.module("entrepotApp", ['ngRoute', 'affichageDev', 'affichageEns', 'creationDevoir', 'effectuerRendu', 'recup', 'modif',
                                                   'loginModule', 'sessionService']);
    
    entrepotApp.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }]);

    entrepotApp.controller('mainController',['Session','$scope','$window',function(Session,$scope,$window) {
        $scope.isLogged = Session.isLogged() ;
        $scope.deconnection = function() {
            Session.resetCredentials();
            $scope.isLogged = false ;
            $window.location.href = "#/entrepot/";
        };

        $scope.$on('loggedIn', function() {
            $scope.isLogged = true ;
        });
    }]);
    
/** ---------- Rooting page/controller ----------  **/
entrepotApp.config(['$routeProvider', '$httpProvider',
  function($routeProvider, $httpProvider) {
      $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
    $routeProvider.
      when('/entrepot/', {
        templateUrl: 'html/login.html',
        controller: 'loginCtrl'
      }).
      when('/entrepot/affichageEleve', {
        templateUrl: 'html/affichage_devoirs_eleve.html',
        controller: 'ListeDevoirsEleve'
      }).
    when('/entrepot/affichageEnseignant', {
        templateUrl: 'html/affichage_rendu_ens.html',
        controller: 'EntrepotListRenduCtrl'
      }).
    when('/entrepot/creerDevoir', {
        templateUrl: 'html/creer_devoir.html',
        controller: 'CreerDevoirCtrl'
      }).
    when('/entrepot/gererRendu/:renduID', {
        templateUrl: 'html/effectuer_rendu_eleve.html',
        controller: 'ListeRendusPrecedents'
      }).
    when('/entrepot/recuperationRendu/:renduID', {
        templateUrl: 'html/recuperation-rendu.html',
        controller: 'EntrepotListEtudiantsCtrl'
      }). 
    when('/entrepot/modifDevoir/:renduID', {
        templateUrl: 'html/modifier_devoir.html',
        controller: 'CreerDevoirCtrl'
      }). 
    when('/entrepot/contact', {
        templateUrl: 'html/contact.html',
        controller: ''
      }).
      otherwise({
        redirectTo: '/entrepot'
      });
  }])
.run(function($rootScope,$location,Session) {
  $rootScope.$on("$routeChangeStart", function(event,next,current) {
    if(Session.getLogin() == ''){
      if(next.templateUrl == "html/login.html" || next.templateUrl == "html/contact.html"){
      } else {
        $location.path("/entrepot/");
      }
    }
    else if(Session.getStatus() == "etudiant"){
      if(next.templateUrl == "html/login.html"){
        $location.path("/entrepot/affichageEleve");
      }
    }
    else if(Session.getStatus() == "enseignant"){
      if(next.templateUrl == "html/login.html"){
        $location.path("/entrepot/affichageEnseignant");
      }
    }
  });
});

  
  
})();
