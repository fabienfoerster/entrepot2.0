(function() {

var affichageEns = angular.module('affichageEns', ['sessionService']);

affichageEns.controller('EntrepotListRenduCtrl', ['$scope','$http','$timeout','Session',function($scope,$http,$timeout,Session) {

    $http.get('http://localhost:8080/jerseySpringJPA/devoir/recupere/all/'+Session.getLogin()).success(function(data){
        console.log(data);
    });
    /*'bouchons/devoirs.json'*/
  $http.get('http://localhost:8080/jerseySpringJPA/devoir/recupere/all/'+Session.getLogin()).success(function(data){
      var res = new Array();
      $scope.nbPending = 0;
      $scope.nbWaiting = 0;
      data.forEach(function(element) {
          if (element.enseignant == Session.getLogin()) {
            res.push(element);
              if(element.state == "pending"){ $scope.nbPending++; }
              if(element.state == "waiting"){ $scope.nbWaiting++; }
          }
    });
      $scope.rendus = res;
    setCountdown();
  });

  var setCountdown = function() {
    for(var i = 0 ; i < $scope.rendus.length; i++){
      $scope.rendus[i].remainingTime = countdown($scope.rendus[i].date) ;
    }
    $timeout(setCountdown , 1000);
  }

  $scope.tab = "pending" ;
  $scope.setTab = function(selectedTab) {
    $scope.tab = selectedTab ;
  };

  $scope.isSet = function(selectedTab) {
    return $scope.tab === selectedTab ;
  };

  var countdown = function(date) {
    return moment().countdown(date).toString();
  };    
    
    
    // Calendrier : decommenter pour ajouter le calendrier des devoirs
  /*  $('#calendar').fullCalendar({
        header:{
            left : 'today prev,next',
            center : 'title',
            right : 'month,agendaWeek,agendaDay'
        },
        events : [
            {
                title: "j2e",
                start: '2014-06-22'
            },
            {
                title: "Myrmes",
                start: '2014-06-22'
            },
            {
                title: "Vidéo de démonstration",
                start: '2014-06-23'
            },
            {
                title: "Myrmes : IA",
                start: '2014-06-21'
            },
            {
                title: "Fiche de specs",
                start: '2014-06-20'
            },
        ]
    })*/

}]);

})();