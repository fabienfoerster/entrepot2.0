(function() {


var effectuerRendu = angular.module('effectuerRendu', ['sessionService', 'angularFileUpload']);


/** ---------- Eleve - Effectuer un rendu ---------- **/

/* Liste des rendus deja effectues */
effectuerRendu.controller('ListeRendusPrecedents', ['$scope','$http','$timeout', 'Session','$routeParams', '$upload',
                                                    function($scope,$http,$timeout, Session,$routeParams, $upload) {

    $scope.renduID = $routeParams.renduID ;

    $http.get('bouchons/'+$scope.renduID+'.json').success(function(data){
        var tmp = data;
        $scope.devoir = tmp;
        setCountdown();

        // Recuperation de la liste des rendus precedemment effectues
        var res = new Array();
        data.rendu_etudiants.forEach(function(element) {
            if (element.login == Session.getLogin() ) { res.push(element.rendus); }
        });
        $scope.rendus_precedents = res[0];
    });

    var setCountdown = function() {
        $scope.devoir.remainingTime = countdown($scope.devoir.date) ;
        $timeout(setCountdown , 1000);
    };

    var countdown = function(date) {
        return moment().countdown(date).toString();
    };

    // Fenetres pop-up en cas de telechargement ou suppression d'un rendu de l'historique
    $scope.popup_telechargement = function() {
        alert("Telechargement du fichier OK");
    };
                                                        
    $scope.suppression = function(tag) {
        // TODO : appel WS
        for(var i = 0; i < $scope.rendus_precedents.length; i++){
            if($scope.rendus_precedents[i].tag == tag){ 
                $scope.rendus_precedents.splice(i, 1);
                break;
            }
        }
    };    
    
    // Ajout de fichiers
    $scope.filesUp = [];
    $scope.invalidFiles = [];
    $scope.onFileSelect = function($files) {
        $scope.invalidFiles = [];
        for (var i = 0; i < $files.length; i++) {
            var file = $files[i];
            if(!contains($scope.filesUp,file)){
                if(matchExt(file.name)){
                    $scope.filesUp.push($files[i]);
                }
                else{
                    $scope.invalidFiles.push($files[i].name);
                }
            }
        }
    };
      
    function matchExt(file){
        for(var i = 0; i < $scope.devoir.files.length; i++){
            var exp = "/*." + $scope.devoir.files[i];
            if(file.match(exp)){ 
                console.log(file.match(exp)); 
                return true; 
            }
        }
        return false;
    }
    
    function contains(a, obj) {
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    }

}]);

})();
