______________________________________________________________

	   Polytech Nice-Sophia Antipolis - 2013/2014
		Sciences Informatiques 4eme annee
		
 Adrien Casanova - Fabien Foerster - Marie-Catherine Turchini
______________________________________________________________	

	   	      Projet innovation
			Entrepot 2.0
		___________________________
		
Application multi-modules Entrepot.

- Se placer dans le dossier et exécuter mvn clean install pour 
compiler/installer le projet et ses modules.

- Se placer dans le dossier orchestrateur et exécuter mvn jetty:run
pour lancer le serveur Jersey.
