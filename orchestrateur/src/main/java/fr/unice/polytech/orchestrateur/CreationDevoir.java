package fr.unice.polytech.orchestrateur;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;

/**
 * Created by haitaar on 19/06/14.
 */
@Component
@Path("/ws")
@Scope("request")
public class CreationDevoir {

   /* @Autowired
    PersonService personService;

    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media
    // type "text/plain"
    @Produces("text/plain")
    public String getIt() {
        Person person = new Person();
        person.setName("David Sells");
        person.setAge(99);

        addIfDoesNotExist(person);

        StringBuffer buffer = new StringBuffer();

        List<Person> persons = personService.getAll();
        for (Person person2 : persons) {
            buffer.append(person2.getName()).append(":").append(person2.getAge())
                    .append("\n");
        }

        return "Hi there: "+buffer.toString();
    }
    private void addIfDoesNotExist(Person person) {
        if(personService.findPerson(person) == null) {
            personService.save(person);
        }
    }*/
	
/*	@EJB
	private DevoirManager devman;

    @GET
    @Path("/grpMat/{matiere}")
    @Produces("text/plain")
    public Response getGrpMat(@PathParam("matiere") String mat){
        IRecuperationGroupesMatiere grpMat = new RecuperationGroupesMatiere();
        String output = grpMat.recupererGroupesMatiere(mat);
        return Response.ok()
                .entity(output)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Content-Type", "text/html; charset=UTF-8")
                .build();
    }
    
    @GET
    @Path("/matEns/{enseignant}")
    @Produces("text/plain")
    public Response getMatEns(@PathParam("enseignant") String ens){
        IRecuperationMatieresEnseignant matEns = new RecuperationMatieresEnseignant();
        String output = matEns.recupererMatieresEnseignant(ens);
        return Response.ok()
                .entity(output)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Content-Type", "text/html; charset=UTF-8")
                .build();
    }

    @OPTIONS
    @Path("/create")
    public Response allowCreateDevoir(){
        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "GET, PUT, OPTIONS, Content-Type")
                .build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createDevoir(DevoirTmp dev){
        System.out.println("* createDevoir avec : "+dev);
        devman.create(dev);
        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "GET, PUT, OPTIONS, Content-Type")
                .build();
    }
    */


}
