package domain;

import java.util.List;

public interface IRecuperationGroupesMatieresSI {
	
	public String recupererGroupesMatiereSI(String libelleMatiere);
	public String recupererGroupesMatiereSI(List<String> libellesMatieres);

}
