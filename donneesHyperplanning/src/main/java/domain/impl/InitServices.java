package domain.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.xml.ws.BindingProvider;

import org.json.JSONArray;
import org.json.JSONObject;

import com.indexeducation.frahtm.hpsvcw_html.HpSvcWAdmin;
import com.indexeducation.frahtm.hpsvcw_html.HpSvcWDonnees;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWAdmin;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWCours;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWEnseignants;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWEtudiants;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWMatieres;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWModulesCursus;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWPromotions;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWRegroupements;
import com.indexeducation.frahtm.hpsvcw_html.IHpSvcWTDOptions;
import com.indexeducation.frahtm.hpsvcw_html.RecupPortCours;
import com.indexeducation.frahtm.hpsvcw_html.RecupPortEnseignant;
import com.indexeducation.frahtm.hpsvcw_html.RecupPortMatieres;
import com.indexeducation.frahtm.hpsvcw_html.RecupPortModulesCursus;
import com.indexeducation.frahtm.hpsvcw_html.RecupPortPromotions;
import com.indexeducation.frahtm.hpsvcw_html.RecupPortRegroupements;
import com.indexeducation.frahtm.hpsvcw_html.RecupPortTDOptions;

public class InitServices {
	
	static IHpSvcWEnseignants lEnseignants;
	static IHpSvcWMatieres lMatieres;
	static IHpSvcWPromotions lPromotions;
	static IHpSvcWModulesCursus lModulesCursus;
	static IHpSvcWCours lCours;
	static IHpSvcWRegroupements lRegroupements;
	static IHpSvcWTDOptions lTDOptions;
	static IHpSvcWEtudiants lEtudiants;
	
	static final long cleMosser = 3079;
    static final long clePinna = 96;
    static long cleSI4;
	static List<Long> tdPartSI4S2;
    
    static HashSet matieresMosser;
    
    
    
    // Constructeur par defaut
    public InitServices() {
    	init();
    }
    
    
    
    private static void init() {
		
		/** ---------- Donnees de connexion  ---------- **/
        String lIdentifiant = "SW";
        String lMotDePasse  = "SEWE";
        
        
        
        /** ---------- Services utilises ---------- **/
        // Donnees
        HpSvcWDonnees lServiceDonnees = new HpSvcWDonnees();
        // Admin
        HpSvcWAdmin lServiceAdmin = new HpSvcWAdmin ();       
        // Enseignant
        RecupPortEnseignant lServiceEnseignant = new RecupPortEnseignant();       
        // Matiere
        RecupPortMatieres lServiceMatiere = new RecupPortMatieres();        
        // Promotion
        RecupPortPromotions lServicePromotion = new RecupPortPromotions();
        // Modules et cursus
        RecupPortModulesCursus lServiceModulesCursus = new RecupPortModulesCursus();
        // Cours
        RecupPortCours lServiceCours = new RecupPortCours();
        // Regroupements
        RecupPortRegroupements lServiceRegroupements = new RecupPortRegroupements();
        // TD/Options
        RecupPortTDOptions lServiceTDOptions = new RecupPortTDOptions();
        
        
        
        /** ---------- Interfaces utilisees ---------- **/        
        // Admin
        IHpSvcWAdmin lAdmin = lServiceAdmin.getPortAdmin();
        ((BindingProvider)lAdmin).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lAdmin).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);       
        // Enseignant
        lEnseignants = lServiceEnseignant.getPortEnseignants();
        ((BindingProvider)lEnseignants).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lEnseignants).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);        
        // Matiere
        lMatieres = lServiceMatiere.getPortMatieres();
        ((BindingProvider)lMatieres).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lMatieres).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);        
        // Promotion
        lPromotions = lServicePromotion.getPortPromotions();
        ((BindingProvider)lPromotions).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lPromotions).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);
        // Modules et cursus
        lModulesCursus = lServiceModulesCursus.getPortModulesCursus();
        ((BindingProvider)lModulesCursus).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lModulesCursus).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);
        // Cours
        lCours = lServiceCours.getPortCours();
        ((BindingProvider)lCours).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lCours).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);
        // Regroupement
        lRegroupements = lServiceRegroupements.getPortRegroupements();
        ((BindingProvider)lRegroupements).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lRegroupements).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);
        // TD et options
        lTDOptions = lServiceTDOptions.getPortTDOptions();
        ((BindingProvider)lTDOptions).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lTDOptions).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);
        // Etudiants
        lEtudiants = lServiceDonnees.getPortEtudiants();
        ((BindingProvider)lEtudiants).getRequestContext().put(BindingProvider.USERNAME_PROPERTY, lIdentifiant);
        ((BindingProvider)lEtudiants).getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, lMotDePasse);
        
        
        
        // Affichage de la version
        System.out.println("Connecte a : " + lAdmin.version());
		
    	cleSI4 = lPromotions.accederPromotionParIdentifiant("SI4");
	}
    
    
    
	/**
	 * Recuperation d'une HashSet de libelle de matieres
	 * @param clesCours, la liste des cles des cours dont on extrait les matieres
	 * @return la liste des matieres sans doublons des cours passes en parametre
	 */
	private HashSet listeMatieresCours(List<Long> clesCours) {
		HashSet res = new HashSet<String>();
        for ( int j = 0 ; j < clesCours.size() ; j++ ) {
        	res.add(lMatieres.libelleMatiere(lCours.matiereCours(clesCours.get(j))));
        }
        return res;
	}

}
