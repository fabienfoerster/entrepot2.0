package domain.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import domain.IRecuperationMatieresEnseignant;

public class RecuperationMatieresEnseignant extends InitServices implements IRecuperationMatieresEnseignant  {
	
	// Constructeur par defaut
	public RecuperationMatieresEnseignant() {
		super();
	}
	
	private List<String> matieresEnseignant(String login) {
		// Recuperation de la cle de l'enseignant \ login
		long cleEns = 0;
		long size = lEnseignants.nombreEnseignants();
		List<Long> liste = lEnseignants.tousLesEnseignants().getTHpSvcWCleEnseignant();
		for ( int i = 0 ; i < size ; i++ ) {
        	long tmp = liste.get(i);
        	if ( lEnseignants.identifiantCASEnseignant(tmp).equals(login) ) {
        		cleEns = tmp;
        		break;
        	}
        }		
		
		// Recuperation de la liste des cours de lenseignant
        List<Long> coursEns = lEnseignants.coursEnseignant(cleEns).getTHpSvcWCleCours();
        HashSet<String> matieresEns = new HashSet<String>();
        for ( int j = 0 ; j < coursEns.size() ; j++ ) {
        	long l = lCours.matiereCours(coursEns.get(j));
        	matieresEns.add(lMatieres.libelleMatiere(l));
        }
        
        // Tri
        List<String> tab = triMatieres(matieresEns);
        
        return tab;
	}
	
	private List<String> matieresEnseignant(Long cleEns) {
		// Recuperation de la liste des cours de lenseignant
        List<Long> coursEns = lEnseignants.coursEnseignant(cleEns).getTHpSvcWCleCours();
        HashSet<String> matieresEns = new HashSet<String>();
        for ( int j = 0 ; j < coursEns.size() ; j++ ) {
        	long l = lCours.matiereCours(coursEns.get(j));
        	matieresEns.add(lMatieres.libelleMatiere(l));
        }
        return triMatieres(matieresEns);
	}
	
	private List<String> triMatieres(HashSet<String> liste) {
		List<String> tabDebut = new ArrayList<String>();
        List<String> tabFin = new ArrayList<String>();
        for ( String s : liste ) {
        	if ( s.contains("commission") || s.contains("Comm.") || s.contains("salles")
        			|| s.contains("R�union") || s.contains("Rentr�e")
        			|| s.contains("rendre notes") || s.contains("Mati�res � pr�ciser") ) {
        		tabFin.add(s);
        	} else {
        		tabDebut.add(s);
        	}
        }
        List<String> tab = new ArrayList<String>();
        tab.addAll(tabDebut);
        tab.add("---------------");
        tab.addAll(tabFin);
        return tab;
	}
	
	private void genereJSONmat(String login, List<String> matieres) throws IOException {
    	final String jsonFilePath = "../jsonFromHyperplanning/MatieresLieesEnseignant/matieres"+login+".json";
    	JSONArray test = new JSONArray();
    	Iterator l = matieres.iterator();
    	while ( l.hasNext() ) {
    		JSONObject o = new JSONObject();
    		o.put("name", l.next());
    		test.put(o);
    	}	
    	Writer file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jsonFilePath),
    															Charset.forName("UTF-8").newEncoder()));
        try {
            file.write(test.toString());
            System.out.println("Successfully Copied JSON Object to File - JSON Object: " + test);
            System.out.println("\tFichier "+jsonFilePath+" g�n�r�.");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            file.flush();
            file.close();
        }
	}
	
	
	
	/**
	 * Generer fichiers JSON des groupes de toutes les matieres de SI
	 */
	public void genererMatieresEnseignant(String login) {
		List<String> matRes = matieresEnseignant(login);

		try {
			genereJSONmat(login, matRes);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void genererMatieresEnseignant(List<String> listLogin) {
		for ( String s : listLogin )
			matieresEnseignant(s);
	}
	
	public void genererMatieresEnseignantCle(List<Long> listeCles) {
		for ( Long l : listeCles ) {
			try {
				genereJSONmat(lEnseignants.identifiantCASEnseignant(l), matieresEnseignant(l));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	

	public String recupererMatieresEnseignant(String loginEnseignant) {
		System.out.println("***** Recuperation des matieres de l'enseignant : "+loginEnseignant+" *****");
		String path = "../jsonFromHyperplanning/MatieresLieesEnseignant/matieres"+loginEnseignant+".json";
		File inputFile = new File(path); 
		StringBuilder res = new StringBuilder();
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader reader = new BufferedReader(in);
			String line = null;
			try {
				while( ( line = reader.readLine() ) != null ) {
					res.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String resFinal = res.toString();
		return resFinal; 
	}
	
	private void genererTousLesFichiers() {
		Date debut = new Date();
    	System.out.println(">>> DEBUT � "+debut);
    	
    	RecuperationMatieresEnseignant test = new RecuperationMatieresEnseignant();
    	
    	List<Long> tousLesEnseignants = new ArrayList<Long>();
    	List<Long> tmp = lEnseignants.tousLesEnseignants().getTHpSvcWCleEnseignant();
    	for ( Long l : tmp ) {
    		if ( !lEnseignants.identifiantCASEnseignant(l).equals(null) )		// Filtre : uniquement si possede id CAS
    			tousLesEnseignants.add(l);
    	}
    	test.genererMatieresEnseignantCle(tousLesEnseignants);
    	
    	Date fin = new Date();
    	System.out.println(">>> FIN � "+fin);
	}

	
	
	public static void main(String[] args) {
        try {
        	RecuperationMatieresEnseignant test = new RecuperationMatieresEnseignant();
//        	test.genererTousLesFichiers();
        	test.genererMatieresEnseignant("mosser");
        	
        } catch(Exception lException) {
            System.out.println("BUG : " + lException.getMessage());
        }
    }
	
}
