package domain.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AutomatisationJSON extends InitServices {
	
	// Services
	private RecuperationMatieresEnseignant recupMatEns;
	private RecuperationGroupesMatieresSI recupGrMat;
//	private RecuperationGroupesMatieres recupGrMat;
	private RecuperationEtudiantsGroupe recupEtudGr;
	
	
	private void creationServices() {
    	recupMatEns = new RecuperationMatieresEnseignant();
    	recupGrMat = new RecuperationGroupesMatieresSI();
//    	recupGrMat = new RecuperationGroupesMatieres();
    	recupEtudGr = new RecuperationEtudiantsGroupe();
	}
	
	private void genererMatieresTousEnseignants() {
		Date debut = new Date();
    	System.out.println(">>> DEBUT � "+debut);
    	
    	RecuperationMatieresEnseignant test = new RecuperationMatieresEnseignant();
    	
    	List<Long> tousLesEnseignants = new ArrayList<Long>();
    	List<Long> tmp = lEnseignants.tousLesEnseignants().getTHpSvcWCleEnseignant();
    	for ( Long l : tmp ) {
    		if ( !lEnseignants.identifiantCASEnseignant(l).equals(null) )		// Filtre : uniquement si possede id CAS
    			tousLesEnseignants.add(l);
    	}
    	test.genererMatieresEnseignantCle(tousLesEnseignants);
    	
    	Date fin = new Date();
    	System.out.println(">>> FIN � "+fin);
	}
	
	private void genererEtudiantsTousGroupes() {
    	Date debut = new Date();
    	System.out.println("Etudiants de tous les groupes\n\t>>> DEBUT � "+debut);
    	
    	List<String> tousLesGroupes = new ArrayList<String>();
    	List<Long> tmp = lTDOptions.tousLesTDOptions().getTHpSvcWCleTDOption();
    	for ( Long l : tmp )
    		tousLesGroupes.add(lTDOptions.nomTDOption(l));
    	recupEtudGr.genererEtudiantsGroupe(tousLesGroupes);
    	
		Date fin = new Date();
    	System.out.println("\t>>> FIN � "+fin);
	}
	
    public static void main(String[] args) {
        try {
        	System.out.println("########## Fichiers JSON en train d'etre generes ... ##########");
        	AutomatisationJSON script = new AutomatisationJSON();
        	
        	// Creation des services
        	script.creationServices();
        	
        	// Generation des fichiers JSON
        	script.genererMatieresTousEnseignants();
        	script.recupGrMat.genererGroupesMatieresSI();
        	script.genererEtudiantsTousGroupes();
        	
        } catch(Exception lException) {
            System.out.println("BUG : " + lException.getMessage());
        }
    }

}
