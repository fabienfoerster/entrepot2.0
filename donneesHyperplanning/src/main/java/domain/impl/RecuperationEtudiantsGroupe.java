package domain.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import domain.IRecuperationEtudiantsGroupe;

public class RecuperationEtudiantsGroupe extends InitServices implements IRecuperationEtudiantsGroupe {
	
	// Constructeur par defaut
	public RecuperationEtudiantsGroupe() {}
	

	/** ----------- Fichiers JSON ----------- **/
	
	public void genererEtudiantsGroupe(String nomGroupe) {
		long cleGR = 0;
		List<Long> tab = lTDOptions.tousLesTDOptions().getTHpSvcWCleTDOption();
		for ( int i = 0 ; i < tab.size() ; i++ ) {
			if ( lTDOptions.nomTDOption(tab.get(i)).equals(nomGroupe) ) {
				cleGR = tab.get(i);
				break;
			}
		}
		List<Long> etudiantsGr = lEtudiants.etudiantsTDOption(cleGR).getTHpSvcWCleEtudiant();

		// Fichier JSON
		String finalNomGroupe = nomGroupe;
		while ( finalNomGroupe.contains("/") ) {
			finalNomGroupe = finalNomGroupe.replace('/', '-');		// Suppression des caracteres posant pb lors de la creation du fichier
		}
		String path = "../jsonFromHyperplanning/EtudiantsLiesGroupe/"+finalNomGroupe+".json";
		
		/**
		 *  TODO Ajouter verification si le fichier existe deja
		 *  Le nom doit correspondre au nom du groupe genere dans RecuperationGroupesMatieres
		 */
		File f = new File(path);
		if ( f.exists() == true ) {
			
		} else {
			
		}
		
		JSONArray test = new JSONArray();
		for ( Long l : etudiantsGr ) {
			JSONObject o = new JSONObject();
			o.put("etudiant", lEtudiants.nomEtudiant(l));
			test.put(o);
		}

		Writer file = null;
		try {
			file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),
																Charset.forName("ISO-8859-1").newEncoder()));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		try {
            file.write(test.toString());
            System.out.println("Successfully Copied JSON Object to File - JSON Object: " + test);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
				file.flush();
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		System.out.println("\tFichier "+path+" g�n�r�.");
	}

	public void genererEtudiantsGroupe(List<String> listeGroupes) {
		System.out.println("***** genererEtudiantsGroupe *****");
		for ( String s : listeGroupes )
			genererEtudiantsGroupe(s);
	}


	
	/** ----------- Envoi du contenu des fichiers JSON demandes sous forme de String ----------- **/
	
	public String recupererEtudiantsGroupe(String nomGroupe) {
		String finalNomGroupe = nomGroupe;
		while ( finalNomGroupe.contains("/") ) {
			finalNomGroupe = finalNomGroupe.replace('/', '-');		// Suppression des caracteres posant pb lors de la creation du fichier
		}
		String path = "../jsonFromHyperplanning/EtudiantsLiesGroupe/"+finalNomGroupe+".json";
		File inputFile = new File(path); 
		StringBuilder res = new StringBuilder();
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader reader = new BufferedReader(in);
			String line = null;
			try {
				while( ( line = reader.readLine() ) != null ) {
					res.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String resFinal = res.toString();
		return resFinal; 
	}
	
	public String recupererEtudiantsGroupe(List<String> listeGroupes) {
		String res = "";
		for ( String s : listeGroupes )
			res += recupererEtudiantsGroupe(s);
		return res;
	}
	
	
	public static void main(String[] args) {
        try {
        	Date debut = new Date();
        	System.out.println(">>> DEBUT � "+debut);
        	
        	RecuperationEtudiantsGroupe test = new RecuperationEtudiantsGroupe();
    		List<String> tousLesGroupes = new ArrayList<String>();
        	List<Long> tmp = lTDOptions.tousLesTDOptions().getTHpSvcWCleTDOption();
        	for ( Long l : tmp )
        		tousLesGroupes.add(lTDOptions.nomTDOption(l));
        	test.genererEtudiantsGroupe(tousLesGroupes);
        	
    		Date fin = new Date();
        	System.out.println(">>> FIN � "+fin);
        } catch(Exception lException) {
            System.out.println("BUG : " + lException.getMessage());
        }
    }
	
}
