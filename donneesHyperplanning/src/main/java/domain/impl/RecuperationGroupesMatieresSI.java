package domain.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import domain.IRecuperationGroupesMatieresSI;

/**
 * Classe permettant la recuperation de la liste des matieres
 * que suivent chaque sous-groupes des promotions de SI.
 * Informations necessaires pour la video de demonstration.
 */

public class RecuperationGroupesMatieresSI extends InitServices implements IRecuperationGroupesMatieresSI {
	
	// Constructeur par defaut
	public RecuperationGroupesMatieresSI() {
		super();
	}
	
	/**
	 * Fonction recuperant les cles des promotions de
	 * SI 3, SI 4 et SI 5
	 * @return liste des cles des promotions de SI
	 */
	private List<Long> recupClesSI() {
		System.out.println("\tR�cup�ration des promotions SI");
		List<Long> res = new ArrayList<Long>();
		
		List<String> promosSI = new ArrayList<String>();
		promosSI.add("SI3"); promosSI.add("SI4"); promosSI.add("SI5");
		for ( int i = 0 ; i < promosSI.size() ; i++ ) {
			// On recupera la cle de la promotion
			long testCle = lPromotions.accederPromotionParIdentifiant(promosSI.get(i));
			// On verifie la validite de la cle
			if ( lPromotions.clePromotionEstValide(testCle) ) {
				res.add(testCle);
			}
		}
		
		return res;
	}
	
	/**
	 * Fonction recuperant la liste des partitions
	 * d'un ensemble de promotions
	 * @param clesPromos, la liste des cles des promotions dont on veut les partitions
	 * @return la liste des cles des partitions de l'ensemble des promotions
	 */
	private List<Long> recupClesPartitions(List<Long> clesPromos) {
		System.out.println("\tR�cup�ration des partitions des promotions SI");
		List<Long> res = new ArrayList<Long>();
		for ( int i = 0 ; i < clesPromos.size() ; i++ ) {
			res.addAll(lTDOptions.partitionsPromotion(clesPromos.get(i)).getTHpSvcWClePartition());
		}
		return res;
	}
	
	/**
	 * Fonction recuperant la liste des partitions (sous-groupes) de TD
	 * pour l'ensemble des partitions passees en parametre
	 * @param clesPartitions, la liste de cles des partitions dont on veut les sous-groupes (partitions TD)
	 * @return la liste des cles des sous groupes de l'ensemble des partitions de promotions passees en parametre
	 */
	private List<Long> recupClesTDPartitions(List<Long> clesPartitions) {
		System.out.println("\tR�cup�ration des partitions TD (sous-groupes) SI");
		List<Long> res = new ArrayList<Long>();
		for ( int i = 0 ; i < clesPartitions.size() ; i++ ) {
			res.addAll(lTDOptions.tdPartition(clesPartitions.get(i)).getTHpSvcWCleTDOption());
		}
		return res;
	}
	
	/**
	 * Fonction recuperant la liste des libelles des matieres
	 * que suivent l'ensemble des partitions de TD (sous-groupes d'une promotion)
	 * @param clesTDPartitions, la liste des cles des sous-groupes dont on veut les matieres suivies
	 * @return la liste des libelles des matieres que suivent l'ensemble des sous-groupes
	 * 				< nom TD partition, List<libelles matieres suivies> >
	 */
	private HashMap<String, List<String>> recupMatieres(List<Long> clesTDPartitions) {
		System.out.println("\tR�cup�ration des mati�res que suivent les promotions SI");
		int i = 1;
		HashMap<String, List<String>> res = new HashMap<String, List<String>>();
		
		// On parcourt la liste des partitions de TD (sous-groupes des promotions)
		for ( long part : clesTDPartitions ) {
			long cleTDPartition = part;																// Cle part.TD
			String nomTDPartition = lTDOptions.nomTDOption(cleTDPartition);							// Nom part.TD
			List<Long> clesCours = lTDOptions.coursTDOption(cleTDPartition).getTHpSvcWCleCours();	// Liste cles cours \ part.TD
			List<String> libellesMatieres = new ArrayList<String>();								// Liste libelles matieres \ part.TD
			// On parcourt la liste des cles des cours de la partition TD
			for ( long cours : clesCours ) {
				long cleMatiere = lCours.matiereCours(cours);										// Cle matiere du cours
				String libMat = lMatieres.libelleMatiere(cleMatiere);								// Libelle de la matiere du cours
				if ( !libellesMatieres.contains(libMat) ) {
					libellesMatieres.add(libMat);
				}
			}
			
			// On ajoute dans la HashMap : < Key=nomPartitionTD, Values=List<libellesMatieresSuivies> >
			if ( res.containsKey(nomTDPartition) ) {
				String stars = "";
				for ( int j = 0; j < i; j++ ) stars += "*";
				String nomBis = nomTDPartition+" "+stars; 
				res.put(nomBis, libellesMatieres);
			} else {
				res.put(nomTDPartition, libellesMatieres);
			}
		}
		return res;
	}
	
	/**
	 * Fonction reversant une liste de la forme : 	<	NomPartitionTD	, 	List<LibellesMatieresSuivies>		>
	 * 		en une liste de la forme			:	<	LibelleMatiere	,	List<NomPartitionTDSuivantMatiere>	>
	 * @param listePartMat
	 * @return
	 */
	private HashMap<String, List<String>> reverseList(HashMap<String, List<String>> listePartMat) {
		System.out.println("\tReverseList");
		HashMap<String, List<String>> res = new HashMap<String, List<String>>();
		for ( String key : listePartMat.keySet() ) {
			List<String> matieres = listePartMat.get(key);
			for ( String m : matieres ) {
				if ( !res.containsKey(m) ) {
					List<String> newList = new ArrayList<String>();
					newList.add(key);
					res.put(m, newList);
				} else {
					res.get(m).add(key);
				}
			}
		}
		return res;
	}
	
	
	
	/**
	 * Genere fichier JSON
	 * @param nomFichier, le nom du fichier JSON a creer
	 * @param liste, la liste a partir de laquelle sera generee le fichier JSON
	 * @param attribut, l'attribut a associer a chaque element
	 * @throws IOException
	 */
	private void genereFichierJSONmatiere(HashMap<String, List<String>> liste) throws IOException {
		System.out.println("\tCr�ation/M�J des fichiers.json contenant la liste des groupes suivant les mati�res ("+liste.size()+" fichiers)");
		System.out.println("\t\tSuccessfully Copied JSON Object to Files :");
		for ( String key : liste.keySet() ) {
			String finalKey = key;
			while ( finalKey.contains("/") ) {
				finalKey = finalKey.replace('/', '-');				// Suppression des caracteres posant pb lors de la creation du fichier
			}
			String path = "jsonFromHyperplanning/GroupesLiesMatiere/"+finalKey+".json";
			
			JSONArray test = new JSONArray();
			List<String> tmp = liste.get(key);
	    	for ( int i = 0 ; i < tmp.size() ; i++ ) {
	    		JSONObject o = new JSONObject();
	    		o.put("gr", tmp.get(i));
	    		test.put(o);
	    	}
	    	
//	    	Writer file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "UTF-8"));
	    	Writer file = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path),
																Charset.forName("ISO-8859-1").newEncoder()));
	        try {
	            file.write(test.toString());
	        } catch (IOException e) {
	        	System.out.println("Erreur dans generefichierJSON");
	            e.printStackTrace();
	        } finally {
	            file.flush();
	            file.close();
	        }
		}
	}
	
	
	/**
	 * Generer fichiers JSON des groupes de toutes les matieres de SI
	 */
	public void genererGroupesMatieresSI() {
		List<Long> clesPromos =	recupClesSI();
		List<Long> clesPartitions = recupClesPartitions(clesPromos);
		List<Long> clesTDPartitions = recupClesTDPartitions(clesPartitions);
		
		HashMap<String, List<String>> tableauPartitionMatieres = recupMatieres(clesTDPartitions);
		HashMap<String, List<String>> tableauMatierePartitions = reverseList(tableauPartitionMatieres);

		try {
			genereFichierJSONmatiere(tableauMatierePartitions);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Recuperation du contenu du fichier JSON de la matiere demandee
	 * @param libelleMatiere
	 * 			Le libelle de la matiere demandee
	 * @return le contenu du fichier JSON sous forme de simple String
	 */
	public String recupererGroupesMatiereSI(String libelleMatiere) {
		String path = "jsonFromHyperplanning/GroupesLiesMatiere/"+libelleMatiere+".json";
		File inputFile = new File(path); 
		StringBuilder res = new StringBuilder();
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader reader = new BufferedReader(in);
			String line = null;
			try {
				while( ( line = reader.readLine() ) != null ) {
					res.append(line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String resFinal = res.toString();
		return resFinal; 
	}
	
	/**
	 * Recuperation du contenu du fichier JSON de la matiere demandee
	 * @param Liste de libelleMatiere
	 * 			La liste des libelles des matieres demandees
	 * @return le contenu du fichier JSON sous forme de simple String
	 */
	public String recupererGroupesMatiereSI(List<String> libellesMatieres) {
		System.out.println("***** recupererGroupesMatiereSI *****");
		String res = "";
		for ( String s : libellesMatieres )
			res += recupererGroupesMatiereSI(s);
		return res;
	}

}
