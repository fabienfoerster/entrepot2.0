package domain;

import java.util.List;

public interface IRecuperationEtudiantsGroupe {
	
	/**
	 * Recuperation de la liste des etudiants contenus dans un groupe
	 * @param nomGroupe
	 * 			identifiant unique du groupe dont on veut la liste des etudiants
	 * @return liste des etudiants du groupe
	 * 			La String sera formee de type JSON
	 */
	public String recupererEtudiantsGroupe(String nomGroupe);
	public String recupererEtudiantsGroupe(List<String> listeGroupes);

}
