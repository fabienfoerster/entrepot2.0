package domain;

public interface IRecuperationGroupesMatiere {
	
	/**
	 * Recuperation des donnees JSON contenant l'ensemble des groupes
	 * suivant une matiere donnee
	 * @param libelleMatiere
	 * 			identifiant unique de la matiere dont on veut la liste des groupes
	 * @return liste des groupes de la matiere
	 * 			La String sera formee de type JSON
	 */
	public String recupererGroupesMatiere(String libelleMatiere);

}
