package domain;

public interface IRecuperationMatieresEnseignant {
	
	/**
	 * Recuperation des donnees JSON contenant l'ensemble des matieres d'un enseignant
	 * @param loginEnseignant
	 * 			identifiant unique de l'enseignant dont on veut la liste des matieres
	 * @return liste des matieres de l'enseignant
	 * 			La String sera formee de type JSON
	 */
	public String recupererMatieresEnseignant(String loginEnseignant);

}
