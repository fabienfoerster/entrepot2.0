
package com.indexeducation.frahtm.hpsvcw;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour THpSvcWTableauClesEnseignants complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="THpSvcWTableauClesEnseignants">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="THpSvcWCleEnseignant" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWCleEnseignant" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "THpSvcWTableauClesEnseignants", propOrder = {
    "tHpSvcWCleEnseignant"
})
public class THpSvcWTableauClesEnseignants {

    @XmlElement(name = "THpSvcWCleEnseignant", type = Long.class)
    protected List<Long> tHpSvcWCleEnseignant;

    /**
     * Gets the value of the tHpSvcWCleEnseignant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tHpSvcWCleEnseignant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTHpSvcWCleEnseignant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getTHpSvcWCleEnseignant() {
        if (tHpSvcWCleEnseignant == null) {
            tHpSvcWCleEnseignant = new ArrayList<Long>();
        }
        return this.tHpSvcWCleEnseignant;
    }

}
