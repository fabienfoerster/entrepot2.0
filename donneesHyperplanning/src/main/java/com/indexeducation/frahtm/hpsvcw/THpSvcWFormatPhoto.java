
package com.indexeducation.frahtm.hpsvcw;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour THpSvcWFormatPhoto.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="THpSvcWFormatPhoto">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="fpJpeg"/>
 *     &lt;enumeration value="fpPng"/>
 *     &lt;enumeration value="fpGif"/>
 *     &lt;enumeration value="fpBitmap"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "THpSvcWFormatPhoto")
@XmlEnum
public enum THpSvcWFormatPhoto {

    @XmlEnumValue("fpJpeg")
    FP_JPEG("fpJpeg"),
    @XmlEnumValue("fpPng")
    FP_PNG("fpPng"),
    @XmlEnumValue("fpGif")
    FP_GIF("fpGif"),
    @XmlEnumValue("fpBitmap")
    FP_BITMAP("fpBitmap");
    private final String value;

    THpSvcWFormatPhoto(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static THpSvcWFormatPhoto fromValue(String v) {
        for (THpSvcWFormatPhoto c: THpSvcWFormatPhoto.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
