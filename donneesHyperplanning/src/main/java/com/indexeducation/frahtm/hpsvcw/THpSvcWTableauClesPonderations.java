
package com.indexeducation.frahtm.hpsvcw;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour THpSvcWTableauClesPonderations complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="THpSvcWTableauClesPonderations">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="THpSvcWClePonderation" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWClePonderation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "THpSvcWTableauClesPonderations", propOrder = {
    "tHpSvcWClePonderation"
})
public class THpSvcWTableauClesPonderations {

    @XmlElement(name = "THpSvcWClePonderation", type = Long.class)
    protected List<Long> tHpSvcWClePonderation;

    /**
     * Gets the value of the tHpSvcWClePonderation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tHpSvcWClePonderation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTHpSvcWClePonderation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Long }
     * 
     * 
     */
    public List<Long> getTHpSvcWClePonderation() {
        if (tHpSvcWClePonderation == null) {
            tHpSvcWClePonderation = new ArrayList<Long>();
        }
        return this.tHpSvcWClePonderation;
    }

}
