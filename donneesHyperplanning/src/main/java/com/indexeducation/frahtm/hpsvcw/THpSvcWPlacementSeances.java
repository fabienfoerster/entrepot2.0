
package com.indexeducation.frahtm.hpsvcw;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour THpSvcWPlacementSeances.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="THpSvcWPlacementSeances">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="mcPlaceFixe"/>
 *     &lt;enumeration value="mcPlaceFixeSeancesMobiles"/>
 *     &lt;enumeration value="mcPlaceVariable"/>
 *     &lt;enumeration value="mcPlaceVariableSeancesMobiles"/>
 *     &lt;enumeration value="mcPlaceVariableSeancesCumulables"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "THpSvcWPlacementSeances")
@XmlEnum
public enum THpSvcWPlacementSeances {

    @XmlEnumValue("mcPlaceFixe")
    MC_PLACE_FIXE("mcPlaceFixe"),
    @XmlEnumValue("mcPlaceFixeSeancesMobiles")
    MC_PLACE_FIXE_SEANCES_MOBILES("mcPlaceFixeSeancesMobiles"),
    @XmlEnumValue("mcPlaceVariable")
    MC_PLACE_VARIABLE("mcPlaceVariable"),
    @XmlEnumValue("mcPlaceVariableSeancesMobiles")
    MC_PLACE_VARIABLE_SEANCES_MOBILES("mcPlaceVariableSeancesMobiles"),
    @XmlEnumValue("mcPlaceVariableSeancesCumulables")
    MC_PLACE_VARIABLE_SEANCES_CUMULABLES("mcPlaceVariableSeancesCumulables");
    private final String value;

    THpSvcWPlacementSeances(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static THpSvcWPlacementSeances fromValue(String v) {
        for (THpSvcWPlacementSeances c: THpSvcWPlacementSeances.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
