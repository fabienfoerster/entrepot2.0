
package com.indexeducation.frahtm.hpsvcw;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour THpSvcWDevise.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * <p>
 * <pre>
 * &lt;simpleType name="THpSvcWDevise">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="mcSymboleEuro"/>
 *     &lt;enumeration value="mcSymboleDollar"/>
 *     &lt;enumeration value="mcSymboleLivre"/>
 *     &lt;enumeration value="mcEuros"/>
 *     &lt;enumeration value="mcCHF"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "THpSvcWDevise")
@XmlEnum
public enum THpSvcWDevise {

    @XmlEnumValue("mcSymboleEuro")
    MC_SYMBOLE_EURO("mcSymboleEuro"),
    @XmlEnumValue("mcSymboleDollar")
    MC_SYMBOLE_DOLLAR("mcSymboleDollar"),
    @XmlEnumValue("mcSymboleLivre")
    MC_SYMBOLE_LIVRE("mcSymboleLivre"),
    @XmlEnumValue("mcEuros")
    MC_EUROS("mcEuros"),
    @XmlEnumValue("mcCHF")
    MC_CHF("mcCHF");
    private final String value;

    THpSvcWDevise(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static THpSvcWDevise fromValue(String v) {
        for (THpSvcWDevise c: THpSvcWDevise.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
