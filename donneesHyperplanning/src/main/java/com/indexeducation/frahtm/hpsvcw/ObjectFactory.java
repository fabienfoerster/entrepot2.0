
package com.indexeducation.frahtm.hpsvcw;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.indexeducation.frahtm.hpsvcw package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.indexeducation.frahtm.hpsvcw
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link THpSvcWTableauDates }
     * 
     */
    public THpSvcWTableauDates createTHpSvcWTableauDates() {
        return new THpSvcWTableauDates();
    }

    /**
     * Create an instance of {@link THpSvcWTableauClesTDOptions }
     * 
     */
    public THpSvcWTableauClesTDOptions createTHpSvcWTableauClesTDOptions() {
        return new THpSvcWTableauClesTDOptions();
    }

    /**
     * Create an instance of {@link THpSvcWTableauClesCours }
     * 
     */
    public THpSvcWTableauClesCours createTHpSvcWTableauClesCours() {
        return new THpSvcWTableauClesCours();
    }

    /**
     * Create an instance of {@link THpSvcWTableauBooleens }
     * 
     */
    public THpSvcWTableauBooleens createTHpSvcWTableauBooleens() {
        return new THpSvcWTableauBooleens();
    }

    /**
     * Create an instance of {@link THpSvcWTableauClesEtudiants }
     * 
     */
    public THpSvcWTableauClesEtudiants createTHpSvcWTableauClesEtudiants() {
        return new THpSvcWTableauClesEtudiants();
    }

    /**
     * Create an instance of {@link THpSvcWTableauClesRegroupements }
     * 
     */
    public THpSvcWTableauClesRegroupements createTHpSvcWTableauClesRegroupements() {
        return new THpSvcWTableauClesRegroupements();
    }

    /**
     * Create an instance of {@link THpSvcWTableauClesPromotions }
     * 
     */
    public THpSvcWTableauClesPromotions createTHpSvcWTableauClesPromotions() {
        return new THpSvcWTableauClesPromotions();
    }

    /**
     * Create an instance of {@link THpSvcWTableauChaines }
     * 
     */
    public THpSvcWTableauChaines createTHpSvcWTableauChaines() {
        return new THpSvcWTableauChaines();
    }

    /**
     * Create an instance of {@link THpSvcWTableauSemaines }
     * 
     */
    public THpSvcWTableauSemaines createTHpSvcWTableauSemaines() {
        return new THpSvcWTableauSemaines();
    }

}
