
package com.indexeducation.frahtm.hpsvcw;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour THpSvcWTableauPlacementsSeances complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="THpSvcWTableauPlacementsSeances">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="THpSvcWPlacementSeances" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWPlacementSeances" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "THpSvcWTableauPlacementsSeances", propOrder = {
    "tHpSvcWPlacementSeances"
})
public class THpSvcWTableauPlacementsSeances {

    @XmlElement(name = "THpSvcWPlacementSeances")
    protected List<THpSvcWPlacementSeances> tHpSvcWPlacementSeances;

    /**
     * Gets the value of the tHpSvcWPlacementSeances property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tHpSvcWPlacementSeances property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTHpSvcWPlacementSeances().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THpSvcWPlacementSeances }
     * 
     * 
     */
    public List<THpSvcWPlacementSeances> getTHpSvcWPlacementSeances() {
        if (tHpSvcWPlacementSeances == null) {
            tHpSvcWPlacementSeances = new ArrayList<THpSvcWPlacementSeances>();
        }
        return this.tHpSvcWPlacementSeances;
    }

}
