
package com.indexeducation.frahtm.hpsvcw;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour THpSvcWTableauTypesSeances complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="THpSvcWTableauTypesSeances">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="THpSvcWTypeSeance" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWTypeSeance" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "THpSvcWTableauTypesSeances", propOrder = {
    "tHpSvcWTypeSeance"
})
public class THpSvcWTableauTypesSeances {

    @XmlElement(name = "THpSvcWTypeSeance")
    protected List<THpSvcWTypeSeance> tHpSvcWTypeSeance;

    /**
     * Gets the value of the tHpSvcWTypeSeance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tHpSvcWTypeSeance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTHpSvcWTypeSeance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THpSvcWTypeSeance }
     * 
     * 
     */
    public List<THpSvcWTypeSeance> getTHpSvcWTypeSeance() {
        if (tHpSvcWTypeSeance == null) {
            tHpSvcWTypeSeance = new ArrayList<THpSvcWTypeSeance>();
        }
        return this.tHpSvcWTypeSeance;
    }

}
