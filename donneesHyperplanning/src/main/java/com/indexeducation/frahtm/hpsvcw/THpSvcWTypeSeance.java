
package com.indexeducation.frahtm.hpsvcw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour THpSvcWTypeSeance complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="THpSvcWTypeSeance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Matiere" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWCleMatiere"/>
 *         &lt;element name="Duree" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWDuree"/>
 *         &lt;element name="TableauEnseignant" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWTableauClesEnseignants"/>
 *         &lt;element name="TableauRegroupement" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWTableauClesRegroupements"/>
 *         &lt;element name="TableauPromotion" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWTableauClesPromotions"/>
 *         &lt;element name="TableauTDOption" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWTableauClesTDOptions"/>
 *         &lt;element name="TableauSalle" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWTableauClesSalles"/>
 *         &lt;element name="PonderationAvantApport" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWClePonderation"/>
 *         &lt;element name="PonderationApresApport" type="{http://www.indexeducation.com/frahtm/HpSvcW.html}THpSvcWClePonderation"/>
 *         &lt;element name="TypeCours" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="JourEtHeureDebut" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "THpSvcWTypeSeance", propOrder = {
    "matiere",
    "duree",
    "tableauEnseignant",
    "tableauRegroupement",
    "tableauPromotion",
    "tableauTDOption",
    "tableauSalle",
    "ponderationAvantApport",
    "ponderationApresApport",
    "typeCours",
    "jourEtHeureDebut"
})
public class THpSvcWTypeSeance {

    @XmlElement(name = "Matiere")
    protected long matiere;
    @XmlElement(name = "Duree")
    protected double duree;
    @XmlElement(name = "TableauEnseignant", required = true)
    protected THpSvcWTableauClesEnseignants tableauEnseignant;
    @XmlElement(name = "TableauRegroupement", required = true)
    protected THpSvcWTableauClesRegroupements tableauRegroupement;
    @XmlElement(name = "TableauPromotion", required = true)
    protected THpSvcWTableauClesPromotions tableauPromotion;
    @XmlElement(name = "TableauTDOption", required = true)
    protected THpSvcWTableauClesTDOptions tableauTDOption;
    @XmlElement(name = "TableauSalle", required = true)
    protected THpSvcWTableauClesSalles tableauSalle;
    @XmlElement(name = "PonderationAvantApport")
    protected long ponderationAvantApport;
    @XmlElement(name = "PonderationApresApport")
    protected long ponderationApresApport;
    @XmlElement(name = "TypeCours", required = true)
    protected String typeCours;
    @XmlElement(name = "JourEtHeureDebut", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar jourEtHeureDebut;

    /**
     * Obtient la valeur de la propri�t� matiere.
     * 
     */
    public long getMatiere() {
        return matiere;
    }

    /**
     * D�finit la valeur de la propri�t� matiere.
     * 
     */
    public void setMatiere(long value) {
        this.matiere = value;
    }

    /**
     * Obtient la valeur de la propri�t� duree.
     * 
     */
    public double getDuree() {
        return duree;
    }

    /**
     * D�finit la valeur de la propri�t� duree.
     * 
     */
    public void setDuree(double value) {
        this.duree = value;
    }

    /**
     * Obtient la valeur de la propri�t� tableauEnseignant.
     * 
     * @return
     *     possible object is
     *     {@link THpSvcWTableauClesEnseignants }
     *     
     */
    public THpSvcWTableauClesEnseignants getTableauEnseignant() {
        return tableauEnseignant;
    }

    /**
     * D�finit la valeur de la propri�t� tableauEnseignant.
     * 
     * @param value
     *     allowed object is
     *     {@link THpSvcWTableauClesEnseignants }
     *     
     */
    public void setTableauEnseignant(THpSvcWTableauClesEnseignants value) {
        this.tableauEnseignant = value;
    }

    /**
     * Obtient la valeur de la propri�t� tableauRegroupement.
     * 
     * @return
     *     possible object is
     *     {@link THpSvcWTableauClesRegroupements }
     *     
     */
    public THpSvcWTableauClesRegroupements getTableauRegroupement() {
        return tableauRegroupement;
    }

    /**
     * D�finit la valeur de la propri�t� tableauRegroupement.
     * 
     * @param value
     *     allowed object is
     *     {@link THpSvcWTableauClesRegroupements }
     *     
     */
    public void setTableauRegroupement(THpSvcWTableauClesRegroupements value) {
        this.tableauRegroupement = value;
    }

    /**
     * Obtient la valeur de la propri�t� tableauPromotion.
     * 
     * @return
     *     possible object is
     *     {@link THpSvcWTableauClesPromotions }
     *     
     */
    public THpSvcWTableauClesPromotions getTableauPromotion() {
        return tableauPromotion;
    }

    /**
     * D�finit la valeur de la propri�t� tableauPromotion.
     * 
     * @param value
     *     allowed object is
     *     {@link THpSvcWTableauClesPromotions }
     *     
     */
    public void setTableauPromotion(THpSvcWTableauClesPromotions value) {
        this.tableauPromotion = value;
    }

    /**
     * Obtient la valeur de la propri�t� tableauTDOption.
     * 
     * @return
     *     possible object is
     *     {@link THpSvcWTableauClesTDOptions }
     *     
     */
    public THpSvcWTableauClesTDOptions getTableauTDOption() {
        return tableauTDOption;
    }

    /**
     * D�finit la valeur de la propri�t� tableauTDOption.
     * 
     * @param value
     *     allowed object is
     *     {@link THpSvcWTableauClesTDOptions }
     *     
     */
    public void setTableauTDOption(THpSvcWTableauClesTDOptions value) {
        this.tableauTDOption = value;
    }

    /**
     * Obtient la valeur de la propri�t� tableauSalle.
     * 
     * @return
     *     possible object is
     *     {@link THpSvcWTableauClesSalles }
     *     
     */
    public THpSvcWTableauClesSalles getTableauSalle() {
        return tableauSalle;
    }

    /**
     * D�finit la valeur de la propri�t� tableauSalle.
     * 
     * @param value
     *     allowed object is
     *     {@link THpSvcWTableauClesSalles }
     *     
     */
    public void setTableauSalle(THpSvcWTableauClesSalles value) {
        this.tableauSalle = value;
    }

    /**
     * Obtient la valeur de la propri�t� ponderationAvantApport.
     * 
     */
    public long getPonderationAvantApport() {
        return ponderationAvantApport;
    }

    /**
     * D�finit la valeur de la propri�t� ponderationAvantApport.
     * 
     */
    public void setPonderationAvantApport(long value) {
        this.ponderationAvantApport = value;
    }

    /**
     * Obtient la valeur de la propri�t� ponderationApresApport.
     * 
     */
    public long getPonderationApresApport() {
        return ponderationApresApport;
    }

    /**
     * D�finit la valeur de la propri�t� ponderationApresApport.
     * 
     */
    public void setPonderationApresApport(long value) {
        this.ponderationApresApport = value;
    }

    /**
     * Obtient la valeur de la propri�t� typeCours.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeCours() {
        return typeCours;
    }

    /**
     * D�finit la valeur de la propri�t� typeCours.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeCours(String value) {
        this.typeCours = value;
    }

    /**
     * Obtient la valeur de la propri�t� jourEtHeureDebut.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJourEtHeureDebut() {
        return jourEtHeureDebut;
    }

    /**
     * D�finit la valeur de la propri�t� jourEtHeureDebut.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJourEtHeureDebut(XMLGregorianCalendar value) {
        this.jourEtHeureDebut = value;
    }

}
