package com.indexeducation.frahtm.hpsvcw_html;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;


@WebServiceClient(name = "HpSvcWDonneesBis", 
                  wsdlLocation = "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWEnseignants",
                  targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html") 
public class RecupPortEnseignant extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "HpSvcWDonnees");
    public final static QName PortEnseignants = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "PortEnseignants");
    
    static {
        URL url = null;
        try {
            url = new URL("http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWEnseignants");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(RecupPortEnseignant.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWEnseignants");
        }
        WSDL_LOCATION = url;
    }

    public RecupPortEnseignant(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public RecupPortEnseignant(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RecupPortEnseignant() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortEnseignant(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortEnseignant(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortEnseignant(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    


    @WebEndpoint(name = "PortEnseignants")
    public IHpSvcWEnseignants getPortEnseignants() {
        return super.getPort(PortEnseignants, IHpSvcWEnseignants.class);
    }


    @WebEndpoint(name = "PortEnseignants")
    public IHpSvcWEnseignants getPortEnseignants(WebServiceFeature... features) {
        return super.getPort(PortEnseignants, IHpSvcWEnseignants.class, features);
    }

}
