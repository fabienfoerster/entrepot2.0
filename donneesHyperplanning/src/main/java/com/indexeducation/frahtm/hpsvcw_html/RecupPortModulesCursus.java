package com.indexeducation.frahtm.hpsvcw_html;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;


@WebServiceClient(name = "HpSvcWDonneesBis", 
                  wsdlLocation = "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWModulesCursus",
                  targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html") 
public class RecupPortModulesCursus extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "HpSvcWDonnees");
    public final static QName PortModulesCursus = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "PortModulesCursus");
    
    static {
        URL url = null;
        try {
            url = new URL("http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWModulesCursus");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(RecupPortModulesCursus.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWModulesCursus");
        }
        WSDL_LOCATION = url;
    }

    public RecupPortModulesCursus(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public RecupPortModulesCursus(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RecupPortModulesCursus() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortModulesCursus(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortModulesCursus(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortModulesCursus(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    


    @WebEndpoint(name = "PortModulesCursus")
    public IHpSvcWModulesCursus getPortModulesCursus() {
        return super.getPort(PortModulesCursus, IHpSvcWModulesCursus.class);
    }


    @WebEndpoint(name = "PortModulesCursus")
    public IHpSvcWModulesCursus getPortModulesCursus(WebServiceFeature... features) {
        return super.getPort(PortModulesCursus, IHpSvcWModulesCursus.class, features);
    }

}
