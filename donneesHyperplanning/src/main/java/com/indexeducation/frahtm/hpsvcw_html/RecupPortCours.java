package com.indexeducation.frahtm.hpsvcw_html;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;


@WebServiceClient(name = "HpSvcWDonneesBis", 
                  wsdlLocation = "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWCours",
                  targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html") 
public class RecupPortCours extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "HpSvcWDonnees");
    public final static QName PortCours = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "PortCours");
    
    static {
        URL url = null;
        try {
            url = new URL("http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWCours");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(RecupPortCours.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWCours");
        }
        WSDL_LOCATION = url;
    }

    public RecupPortCours(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public RecupPortCours(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RecupPortCours() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortCours(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortCours(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortCours(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    


    @WebEndpoint(name = "PortCours")
    public IHpSvcWCours getPortCours() {
        return super.getPort(PortCours, IHpSvcWCours.class);
    }


    @WebEndpoint(name = "PortCours")
    public IHpSvcWCours getPortCours(WebServiceFeature... features) {
        return super.getPort(PortCours, IHpSvcWCours.class, features);
    }

}
