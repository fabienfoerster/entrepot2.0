package com.indexeducation.frahtm.hpsvcw_html;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;


@WebServiceClient(name = "HpSvcWDonneesBis", 
                  wsdlLocation = "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWRegroupements",
                  targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html") 
public class RecupPortRegroupements extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "HpSvcWDonnees");
    public final static QName PortRegroupements = new QName("http://www.indexeducation.com/frahtm/HpSvcW.html", "PortRegroupements");
    
    static {
        URL url = null;
        try {
            url = new URL("http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWRegroupements");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(RecupPortRegroupements.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://sco.polytech.unice.fr:8080/hpsw/wsdl/IHpSvcWRegroupements");
        }
        WSDL_LOCATION = url;
    }

    public RecupPortRegroupements(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public RecupPortRegroupements(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RecupPortRegroupements() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortRegroupements(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortRegroupements(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public RecupPortRegroupements(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }    


    @WebEndpoint(name = "PortRegroupements")
    public IHpSvcWRegroupements getPortRegroupements() {
        return super.getPort(PortRegroupements, IHpSvcWRegroupements.class);
    }


    @WebEndpoint(name = "PortRegroupements")
    public IHpSvcWRegroupements getPortRegroupements(WebServiceFeature... features) {
        return super.getPort(PortRegroupements, IHpSvcWRegroupements.class, features);
    }

}
