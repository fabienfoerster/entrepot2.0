package com.indexeducation.frahtm.hpsvcw_html;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.0.0
 * 2014-06-11T11:11:34.602+02:00
 * Generated source version: 3.0.0
 * 
 */
@WebService(targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", name = "IHpSvcWAdmin")
@XmlSeeAlso({com.indexeducation.frahtm.hpsvcw.ObjectFactory.class})
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IHpSvcWAdmin {

    /**
     * Devise utilisée pour les coûts horaires
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "DeviseCoutsHoraires", action = "urn:HpSvcW2014:IHpSvcWAdmin#DeviseCoutsHoraires")
    public com.indexeducation.frahtm.hpsvcw.THpSvcWDevise deviseCoutsHoraires();

    /**
     * Permet de coder le format désiré du texte
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "FormatTexteICAL", action = "urn:HpSvcW2014:IHpSvcWAdmin#FormatTexteICAL")
    public long formatTexteICAL(
        @WebParam(partName = "AAvecLibelle", name = "AAvecLibelle")
        boolean aAvecLibelle,
        @WebParam(partName = "AAvecCode", name = "AAvecCode")
        boolean aAvecCode,
        @WebParam(partName = "AAvecLibelleLong", name = "AAvecLibelleLong")
        boolean aAvecLibelleLong
    );

    /**
     * Enlève au tableau de semaines, la semaine passée en paramètre
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "EnleverSemaineAuTableauSemaines", action = "urn:HpSvcW2014:IHpSvcWAdmin#EnleverSemaineAuTableauSemaines")
    public com.indexeducation.frahtm.hpsvcw.THpSvcWTableauSemaines enleverSemaineAuTableauSemaines(
        @WebParam(partName = "ASemaine", name = "ASemaine")
        int aSemaine,
        @WebParam(partName = "ATableau", name = "ATableau")
        com.indexeducation.frahtm.hpsvcw.THpSvcWTableauSemaines aTableau
    );

    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "Version", action = "urn:HpSvcW2014:IHpSvcWAdmin#Version")
    public java.lang.String version();

    /**
     * Convertis en HpSvcWCouleur les 3 valeurs (rouge, vert et bleu) comprises entre 0 et 255
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "RGBEnHpSvcWCouleur", action = "urn:HpSvcW2014:IHpSvcWAdmin#RGBEnHpSvcWCouleur")
    public int rgbEnHpSvcWCouleur(
        @WebParam(partName = "R", name = "R")
        short r,
        @WebParam(partName = "G", name = "G")
        short g,
        @WebParam(partName = "B", name = "B")
        short b
    );

    /**
     * Convertis une durée HpSvcWDuree en 2 valeurs heure/minute passées en paramètre OUT.
     */
    @WebMethod(operationName = "HpSvcWDureeEnHeureMinute", action = "urn:HpSvcW2014:IHpSvcWAdmin#HpSvcWDureeEnHeureMinute")
    public void hpSvcWDureeEnHeureMinute(
        @WebParam(partName = "ADuree", name = "ADuree")
        double aDuree,
        @WebParam(partName = "AHeure", mode = WebParam.Mode.OUT, name = "AHeure")
        javax.xml.ws.Holder<java.lang.Short> aHeure,
        @WebParam(partName = "AMinute", mode = WebParam.Mode.OUT, name = "AMinute")
        javax.xml.ws.Holder<java.lang.Short> aMinute
    );

    /**
     * retourne un tableau contenant toutes les semaines de l'année
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "AnneeComplete", action = "urn:HpSvcW2014:IHpSvcWAdmin#AnneeComplete")
    public com.indexeducation.frahtm.hpsvcw.THpSvcWTableauSemaines anneeComplete();

    /**
     * Convertis en HpSvcWDuree la durée passée en heure/minute
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "HeureMinuteEnHpSvcWDuree", action = "urn:HpSvcW2014:IHpSvcWAdmin#HeureMinuteEnHpSvcWDuree")
    public double heureMinuteEnHpSvcWDuree(
        @WebParam(partName = "AHeure", name = "AHeure")
        short aHeure,
        @WebParam(partName = "AMinute", name = "AMinute")
        short aMinute
    );

    /**
     * Permet de coder le format désiré du texte des enseignants
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "FormatTexteICALEnseignant", action = "urn:HpSvcW2014:IHpSvcWAdmin#FormatTexteICALEnseignant")
    public long formatTexteICALEnseignant(
        @WebParam(partName = "AAvecNom", name = "AAvecNom")
        boolean aAvecNom,
        @WebParam(partName = "AAvecNomMajuscule", name = "AAvecNomMajuscule")
        boolean aAvecNomMajuscule,
        @WebParam(partName = "AAvecPrenomComplet", name = "AAvecPrenomComplet")
        boolean aAvecPrenomComplet,
        @WebParam(partName = "AAvecPrenomInitiale", name = "AAvecPrenomInitiale")
        boolean aAvecPrenomInitiale,
        @WebParam(partName = "AAvecCode", name = "AAvecCode")
        boolean aAvecCode,
        @WebParam(partName = "AAvecCivilite", name = "AAvecCivilite")
        boolean aAvecCivilite
    );

    /**
     * Convertis HpSvcWCouleur en 3 valeurs (rouge, vert et bleu) passées en paramètre OUT.
     */
    @WebMethod(operationName = "HpSvcWCouleurEnRGB", action = "urn:HpSvcW2014:IHpSvcWAdmin#HpSvcWCouleurEnRGB")
    public void hpSvcWCouleurEnRGB(
        @WebParam(partName = "ACouleur", name = "ACouleur")
        int aCouleur,
        @WebParam(partName = "R", mode = WebParam.Mode.OUT, name = "R")
        javax.xml.ws.Holder<java.lang.Short> r,
        @WebParam(partName = "G", mode = WebParam.Mode.OUT, name = "G")
        javax.xml.ws.Holder<java.lang.Short> g,
        @WebParam(partName = "B", mode = WebParam.Mode.OUT, name = "B")
        javax.xml.ws.Holder<java.lang.Short> b
    );

    /**
     * Ajoute au tableau de semaines, la semaine passée en paramètre
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "AjouterSemaineAuTableauSemaines", action = "urn:HpSvcW2014:IHpSvcWAdmin#AjouterSemaineAuTableauSemaines")
    public com.indexeducation.frahtm.hpsvcw.THpSvcWTableauSemaines ajouterSemaineAuTableauSemaines(
        @WebParam(partName = "ASemaine", name = "ASemaine")
        int aSemaine,
        @WebParam(partName = "ATableau", name = "ATableau")
        com.indexeducation.frahtm.hpsvcw.THpSvcWTableauSemaines aTableau
    );

    /**
     * retourne un tableau contenant la semaine passée en paramètre
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "SemaineEnTableauSemaines", action = "urn:HpSvcW2014:IHpSvcWAdmin#SemaineEnTableauSemaines")
    public com.indexeducation.frahtm.hpsvcw.THpSvcWTableauSemaines semaineEnTableauSemaines(
        @WebParam(partName = "ASemaine", name = "ASemaine")
        int aSemaine
    );

    /**
     * Modifie la devise utilisée pour les coûts horaires
     */
    @WebMethod(operationName = "ModifierDeviseCoutsHoraires", action = "urn:HpSvcW2014:IHpSvcWAdmin#ModifierDeviseCoutsHoraires")
    public void modifierDeviseCoutsHoraires(
        @WebParam(partName = "ADevise", name = "ADevise")
        com.indexeducation.frahtm.hpsvcw.THpSvcWDevise aDevise
    );

    /**
     * Date du premier jour dans la base
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "DatePremierJourBase", action = "urn:HpSvcW2014:IHpSvcWAdmin#DatePremierJourBase")
    public javax.xml.datatype.XMLGregorianCalendar datePremierJourBase();

    /**
     * Durée totale sur le domaine 'ADomaine'.
     */
    @WebResult(name = "return", targetNamespace = "http://www.indexeducation.com/frahtm/HpSvcW.html", partName = "return")
    @WebMethod(operationName = "DureeTotale", action = "urn:HpSvcW2014:IHpSvcWAdmin#DureeTotale")
    public double dureeTotale(
        @WebParam(partName = "ADomaine", name = "ADomaine")
        com.indexeducation.frahtm.hpsvcw.THpSvcWTableauSemaines aDomaine
    );
}
