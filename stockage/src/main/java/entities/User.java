package entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "USERS")
public class User {
	
	private String login;
	
	public User() {}

	@Id
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
	
	
	
	public String toString(){
        StringBuilder res = new StringBuilder();
        res.append(" # USER > "+login);
        return res.toString();
    }
	
}
