package entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by haitaar on 19/06/14.
 */
@Entity
@Table(name = "DEVOIRS")
public class Devoir {

    private Long id;
    private String matiere;
    private String name;
    private List<User> eleves;
    private String date;
    private String heure;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @ElementCollection
    public List<User> getEleves() {
        return eleves;
    }
    public void setEleves(List<User> newEleves) {
        this.eleves = newEleves;
    }

    public String getMatiere() {
        return matiere;
    }
    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }
    
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getHeure() {
		return heure;
	}
	public void setHeure(String heure) {
		this.heure = heure;
	}


    public String toString(){
        StringBuilder res = new StringBuilder();
        res.append("Devoir : \n");
        res.append("Matiere : "+matiere+"\n");
        res.append("Nom : "+name+"\n");
        res.append("Eleves : "+eleves);
        return res.toString();
    }

}
