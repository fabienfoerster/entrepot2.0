package entities;

import java.util.List;

public class DevoirTmp {
	
    private String matiere;
    private String name;
    private boolean checked;
    private List<Groupe> groupes = null;
    private String date = null;
    private String heure = null;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
	public boolean isChecked() {
		return checked;
	}
	public boolean getChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

    public List<Groupe> getGroupes() {
        return groupes;
    }
    public void setGroupes(List<Groupe> newGroupes) {
        this.groupes = newGroupes;
    }

    public String getMatiere() {
        return matiere;
    }
    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }
    
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getHeure() {
		return heure;
	}
	public void setHeure(String heure) {
		this.heure = heure;
	}


    public String toString(){
        StringBuilder res = new StringBuilder();
        res.append("--- Devoir temporaire ---\n");
        res.append("Matiere : "+matiere+"\n");
        res.append("Nom : "+name+"\n");
        res.append("Checked ? "+checked+"\n");
        res.append("Groupes : "+groupes+"\n");
        res.append("Date : "+date+"\n");
        res.append("Heure : "+heure);
        return res.toString();
    }


}
