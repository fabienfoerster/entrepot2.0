package converters;

import com.google.gson.Gson;
import entities.Devoir;
import entities.User;

/**
 * Created by haitaar on 19/06/14.
 */
public class JsonToJava {

    public Devoir convertInDevoir(String json){
        Gson gson = new Gson();
        Devoir dev = gson.fromJson(json, Devoir.class);
        return dev;
    }
    
    public User convertInUser(String json){
        Gson gson = new Gson();
        User u = gson.fromJson(json, User.class);
        return u;
    }
}
