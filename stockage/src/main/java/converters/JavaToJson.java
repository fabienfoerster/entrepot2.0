package converters;

import com.google.gson.Gson;
import entities.Devoir;
import entities.User;

/**
 * Created by haitaar on 19/06/14.
 */
public class JavaToJson {

    public String convertDevoir(Devoir dev){
        Gson gson = new Gson();
        return gson.toJson(dev);
    }
    
    public String convertUser(User u){
        Gson gson = new Gson();
        return gson.toJson(u);
    }
}
