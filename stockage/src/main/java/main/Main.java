package main;

import converters.JavaToJson;
import converters.JsonToJava;
import entities.Devoir;
import entities.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haitaar on 19/06/14.
 */
public class Main {

    public static void main(String[] args){
    	
    	/** ##### DEVOIR ##### **/
        /* Json To Java */
        String devJson = "{'name' : 'Myrmes', 'matiere' : 'OGL', 'groupes' : ['G1', 'G2']}";
        
        JsonToJava jsonToJava = new JsonToJava();
        System.out.println(jsonToJava.convertInDevoir(devJson));

        
        /* Java To Json */
        Devoir dev = new Devoir();
        dev.setName("DevTest");
        dev.setMatiere("MatTest");
        List<String> groupes = new ArrayList<String>();
        groupes.add("G1");
        groupes.add("G2");

        JavaToJson javaToJson = new JavaToJson();
        System.out.println(javaToJson.convertDevoir(dev)+"\n");
        
        
        
        /** ##### USER ##### **/
        /* Json To Java */
        String userJson = "{'login' : 'acasanov'}";
        System.out.println(jsonToJava.convertInUser(userJson));
        
        /* Java To Json */
        User user = new User();
        user.setLogin("toto");
        System.out.println(javaToJson.convertUser(user));
        
    }

}
