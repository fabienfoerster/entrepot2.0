package domain.impl;

import java.util.ArrayList;
import java.util.List;

import converters.JavaToJson;
import domain.DevoirManager;
import domain.IRecuperationEtudiantsGroupe;
import domain.IRecuperationGroupesMatiere;
import domain.UserManager;
import entities.Devoir;
import entities.DevoirTmp;
import entities.Groupe;
import entities.User;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.ejb.EJB;

/**
 * Created by haitaar on 19/06/14.
 */
@Stateless
@Local(DevoirManager.class)
public class DevoirManagerBean implements DevoirManager {

    private JavaToJson javaToJson;

    @PersistenceContext
    EntityManager entityManager;
    
    IRecuperationGroupesMatiere recupGrMat;
    IRecuperationEtudiantsGroupe recupEtudGr;
    
    @EJB
    UserManager userManager;
    

    
    /** ---------- ---------- CREATION ---------- ---------- **/
    
    @Override
//    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Devoir> create(DevoirTmp devTmp) {
    	System.out.println("\t##### Create devoir");
    	recupGrMat = new RecuperationGroupesMatiere();
    	recupEtudGr = new RecuperationEtudiantsGroupe();
    	
    	List<Devoir> res = new ArrayList<Devoir>();
    	
    	if ( devTmp.isChecked() == false ) {		// Si selection = tous
    		res.add(this.createTous(devTmp));
    	} else {									// Si selection = groupes
    		res.addAll(this.createGroupes(devTmp));
    	}
    	
    	return res;
    	
    }
    
    /**
     * [annexe]
     * Fonction creant et persistant un devoir dans le cas ou
     * l'enseignant a selectionne "tous", soit un unique devoir pour la matiere
     * @param devTmp
     */
    protected Devoir createTous(DevoirTmp devTmp) {
    	System.out.println("\t\t\tTEST create pour tous");
    	// Creation "manuelle" de la liste des groupes
    	String grMatS = recupGrMat.recupererGroupesMatiere(devTmp.getMatiere());
		JSONArray grMatJSON = new JSONArray(grMatS);
		List<Groupe> listeGroupes = new ArrayList<Groupe>();
		for ( int i = 0 ; i < grMatJSON.length() ; i++ ) {
			Groupe g = new Groupe();
			String s = (String) ((JSONObject) grMatJSON.get(i)).get("gr");
			g.setName(s);
			g.setDate(devTmp.getDate());
			g.setHeure(devTmp.getHeure());
			listeGroupes.add(g);
		}
		devTmp.setGroupes(listeGroupes);
		System.out.println("\t\t\tTEST apr�s groupes");
		
		// Recuperation de la liste des eleves
    	List<User> listeEleves = this.recupEtudiants(devTmp.getGroupes());
		
    	System.out.println("\t\t\tTEST avant persist");
		// Persistance du devoir
		Devoir devFinal = new Devoir();
    	devFinal.setMatiere(devTmp.getMatiere());
    	devFinal.setName(devTmp.getName());
    	devFinal.setEleves(listeEleves);
    	devFinal.setDate(devTmp.getDate());
    	devFinal.setHeure(devTmp.getHeure());
        entityManager.persist(devFinal);
        System.out.println("\t\t[tous] Persistance de : "+devFinal);
        return devFinal;
    }
    
    /**
     * [annexe]
     * Fonction creant et persistant un devoir dans le cas ou
     * l'enseignant a selectionne "groupes", soit un devoir par groupe
     * avec des dates et heures differentes de rendus
     * @param devTmp
     */
    protected List<Devoir> createGroupes(DevoirTmp devTmp) {
    	List<Devoir> res = new ArrayList<Devoir>();
    	
    	for (Groupe g : devTmp.getGroupes() ) {
    		// Recuperation de la liste des eleves
    		List<Groupe> listtmp = new ArrayList<Groupe>();
    		listtmp.add(g);
        	List<User> listeEleves = this.recupEtudiants(listtmp);
    	
    		// Persistance des devoirs
    		Devoir devFinal = new Devoir();
        	devFinal.setMatiere(devTmp.getMatiere());
        	devFinal.setName(devTmp.getName());
        	devFinal.setEleves(listeEleves);
        	devFinal.setDate(g.getDate());
        	devFinal.setHeure(g.getHeure());
            entityManager.persist(devFinal);
            System.out.println("\t\t[groupes] Persistance de : "+devFinal);
            res.add(devFinal);
    	}
    	
    	return res;
		
    }
    
    /**
     * [annexe]
     * Recuperation de la liste de User pour une liste de Groupe donnee
     * @param listeGr
     * @return
     */
    protected List<User> recupEtudiants(List<Groupe> listeGr) {
    	List<User> res = new ArrayList<User>();
    	for ( Groupe g : listeGr ) {
    		JSONArray et1 = new JSONArray(recupEtudGr.recupererEtudiantsGroupe(g.getName()));
    		for ( int i = 0 ; i < et1.length() ; i++ ) {
    			String s = (String) ((JSONObject) et1.get(i)).get("etudiant");
    			System.out.println("++++ TEST : "+s);
    			User u = new User();
    			u.setLogin(s);
    			if ( userManager.findByLogin(s) == null ) {
    				entityManager.persist(u);
    			}
    			res.add(u);
    			entityManager.merge(u);
    		}
    	}   	
    	return res;
    }

    /** ---------- ---------- ---------- ---------- ---------- **/
    
    
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Devoir findById(Long id) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Devoir> criteria = builder.createQuery(Devoir.class);
        Root<Devoir> from = criteria.from(Devoir.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("id"), id));
        TypedQuery<Devoir> query = entityManager.createQuery(criteria);
        try {
            return query.getSingleResult();
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void remove(Devoir dev) {
        dev = entityManager.merge(dev);
        try {
            entityManager.remove(dev);
        } catch(Exception e){
            throw new RuntimeException();
        }

    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public String getDevoirJson(Long id) {
        Devoir dev = findById(id);
        javaToJson = new JavaToJson();
        return javaToJson.convertDevoir(dev);
    }
}
