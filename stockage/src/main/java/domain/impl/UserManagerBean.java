package domain.impl;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import converters.JsonToJava;

import domain.UserManager;

import entities.User;

@Stateless
@Local(UserManager.class)
public class UserManagerBean implements UserManager {
	
	private JsonToJava jsonToJava;
    
    @PersistenceContext
    EntityManager entityManager;
	
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
	public User create(String json) {
    	jsonToJava = new JsonToJava();
        User u = jsonToJava.convertInUser(json);
        entityManager.persist(u);
        return u;
	}
	
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public User findByLogin(String login) {
    	CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> from = criteria.from(User.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("login"), login));
        TypedQuery<User> query = entityManager.createQuery(criteria);
        try {
        	int res = query.getResultList().size();
        	if ( res == 0 )
        		return null;
        	else
        		return query.getSingleResult();
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void remove(User user) {
    	user = entityManager.merge(user);
        try {
            entityManager.remove(user);
        } catch(Exception e){
            throw new RuntimeException();
        }
    }

}
