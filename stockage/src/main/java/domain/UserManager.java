package domain;

import entities.User;

public interface UserManager {
	
	public User create(String json);
    public User findByLogin(String login);
    public void remove(User dev);

}
