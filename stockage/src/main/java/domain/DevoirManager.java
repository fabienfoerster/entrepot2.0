package domain;

import java.util.List;

import entities.Devoir;
import entities.DevoirTmp;

/**
 * Created by haitaar on 19/06/14.
 */
public interface DevoirManager {

    public List<Devoir> create(DevoirTmp devTmp);
    public Devoir findById(Long id);
    public void remove(Devoir dev);
    public String getDevoirJson(Long id);
}
