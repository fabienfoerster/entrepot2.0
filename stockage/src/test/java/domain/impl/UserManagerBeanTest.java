package domain.impl;

import javax.ejb.EJB;

import junit.framework.TestCase;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import domain.UserManager;
import entities.User;

@RunWith(Arquillian.class)
public class UserManagerBeanTest extends TestCase {
	
    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(User.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource("META-INF/persistence.xml", "persistence.xml")
                .addPackage(UserManager.class.getPackage())
                .addPackage(UserManagerBean.class.getPackage());
    }

    @EJB
    private UserManager manager;

    private User userTest;
    private String userJson;

    @Before
    public void setUp() {
    	userJson = "{'login' : 'foerster'}";
        userTest = manager.create(userJson);
    }

    @After
    public void cleanUp() {
        manager.remove(userTest);
        userTest = null;
    }

    @Test
    public void testCreate() throws Exception {
        User user = manager.findByLogin(userTest.getLogin());
        assertEquals("foerster", user.getLogin());
    }

    @Test
    public void testFindById() throws Exception {
        assertNotNull(manager.findByLogin(userTest.getLogin()));
        assertNull(manager.findByLogin("nexistepas"));
    }

}
