package domain.impl;

import java.util.ArrayList;
import java.util.List;

import domain.DevoirManager;
import entities.Devoir;
import entities.DevoirTmp;
import entities.Groupe;
import junit.framework.TestCase;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;

/**
 * Created by haitaar on 19/06/14.
 */
@RunWith(Arquillian.class)
public class DevoirManagerBeanTest extends TestCase {
	
    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Devoir.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource("META-INF/persistence.xml", "persistence.xml")
                .addPackage(DevoirManager.class.getPackage())
                .addPackage(DevoirManagerBean.class.getPackage());
    }

    @EJB
    private DevoirManager manager;

    private DevoirTmp devTest1, devTest2;
    private Devoir devFinal1, devFinal2;

    @Before
    public void setUp() {
        devTest1 = new DevoirTmp();
        devTest1.setName("TEST1");
        devTest1.setMatiere("Serveurs d'entreprise");
        devTest1.setChecked(false);
        devTest1.setGroupes(null);
        
        devTest2 = new DevoirTmp();
        devTest2.setName("TEST2");
        devTest2.setMatiere("Serveurs d'entreprise");
        devTest2.setChecked(true);
        List<Groupe> listeGr = new ArrayList<Groupe>();
        Groupe g = new Groupe();
        g.setName("SAR1");
        g.setDate("01/07/2014");
        g.setHeure("23:55");
        listeGr.add(g);
        Groupe g2 = new Groupe();
        g2.setName("LOG1");
        g2.setDate("03/07/2014");
        g2.setHeure("23:55");
        listeGr.add(g2);
        devTest2.setGroupes(listeGr);
    }

    @After
    public void cleanUp() {
    	manager.remove(devFinal1);
    	manager.remove(devFinal2);
    }

    @Test
    public void testCreate() throws Exception {
        List<Devoir> res1 = manager.create(devTest1);
        devFinal1 = res1.get(0);
        assertEquals("TEST1", devFinal1.getName());
        
        assertEquals(2, devTest2.getGroupes().size());
        List<Devoir> res2 = manager.create(devTest2);
        assertEquals(2, res2.size());
    }
	

}
