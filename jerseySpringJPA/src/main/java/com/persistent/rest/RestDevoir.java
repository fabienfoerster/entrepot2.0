package com.persistent.rest;

import com.persistent.entity.Devoir;
import com.persistent.entity.DevoirTmp;
import com.persistent.entity.Groupe;
import com.persistent.service.DevoirService;
import domain.IRecuperationGroupesMatiere;
import domain.IRecuperationMatieresEnseignant;
import domain.impl.RecuperationGroupesMatiere;
import domain.impl.RecuperationMatieresEnseignant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by haitaar on 24/06/14.
 */
@Path("/devoir")
@Component
@Scope("request")
public class RestDevoir {

    @Autowired
    DevoirService devoirService;

    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media
    // type "text/plain"
    @Produces("text/plain")
    public Response getIt() {
        Devoir devoir = new Devoir();
        devoir.setName("EJB");
        devoir.setMatiere("SE");

        addIfDoesNotExist(devoir);

        StringBuffer buffer = new StringBuffer();

        List<Devoir> devoirs = devoirService.getAll();
        for (Devoir dev : devoirs) {
            System.out.println(dev.getMatiere());
            buffer.append(dev.getMatiere()).append(":").append(dev.getName())
                    .append("\n");
        }

        String output = "Hi there: "+buffer.toString();
        return Response.ok()
                .entity(output)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Content-Type", "text/html; charset=UTF-8")
                .build();
    }

    @GET
    @Path("/matEns/{enseignant}")
    @Produces("text/plain")
    public Response getMatEns(@PathParam("enseignant") String ens){
        IRecuperationMatieresEnseignant matEns = new RecuperationMatieresEnseignant();
        String output = matEns.recupererMatieresEnseignant(ens);
        return Response.ok()
                .entity(output)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Content-Type", "text/html; charset=UTF-8")
                .build();
    }

    @GET
    @Path("/grpMat/{matiere}")
    @Produces("text/plain")
    public Response getGrpMat(@PathParam("matiere") String mat){
        IRecuperationGroupesMatiere grpMat = new RecuperationGroupesMatiere();
        String output = grpMat.recupererGroupesMatiere(mat);
        return Response.ok()
                .entity(output)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Content-Type", "text/html; charset=UTF-8")
                .build();
    }

    @OPTIONS
    @Path("/create")
    public Response allowCreateDevoir(){
        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "GET, PUT, OPTIONS, Content-Type")
                .build();
    }

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createDevoir(DevoirTmp devTmp){
        persistDevoir(devTmp);
        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "GET, PUT, OPTIONS, Content-Type")
                .build();
    }

    private void persistDevoir(DevoirTmp devTmp){
        String name = devTmp.getName();
        String matiere = devTmp.getMatiere();
        String date = devTmp.getDate();
        String heure = devTmp.getHeure();
        String enseignant = devTmp.getEnseignant();
        String description = devTmp.getDescription();
        List<String> extensions = devTmp.getExtensions();
        Devoir devoir;
        if(devTmp.getChecked()){
            for(Groupe grp : devTmp.getGroupes()){
                devoir = setAttributesDevoir(matiere, name, grp.getDate(), grp.getHeure(), enseignant, description, grp.getName(), extensions);
                addIfDoesNotExist(devoir);
            }
        }
        else{
            devoir = setAttributesDevoir(matiere, name, date, heure,
                    enseignant, description, "", extensions);
            addIfDoesNotExist(devoir);
        }
    }

    private Devoir setAttributesDevoir(String matiere, String name, String date,String heure,String enseignant,
                                       String description, String groupe, List<String> extensions){
        Devoir devoir = new Devoir();
        devoir.setMatiere(matiere);
        devoir.setName(name);
        devoir.setDate(date);
        devoir.setHeure(heure);
        devoir.setEnseignant(enseignant);
        devoir.setDescription(description);
        devoir.setGroupe(groupe);
        devoir.setState("pending");
        StringBuilder str = new StringBuilder();
        if(extensions.size() > 0){
            for(String ext : extensions){
                str.append(ext).append(";");
            }
            devoir.setExtensions(str.toString());
        }
        else{
            devoir.setExtensions("*");
        }
        return devoir;
    }

    @GET
    @Path("/recupere/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response recupererDevoir(@PathParam("id") int id){
        Devoir devoir = devoirService.getById(id);
        return Response.ok()
                .entity(devoir)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "GET, PUT, OPTIONS, Content-Type")
                .build();
    }

    @GET
    @Path("/recupere/all/{ens}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response recupereDevoirs(@PathParam("ens") String enseignant){
        List<Devoir> devoirs = devoirService.getByEns(enseignant);
        return Response.ok()
                .entity(devoirs)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "GET, PUT, OPTIONS, Content-Type")
                .build();
    }

    @GET
    @Path("/delete/{id}")
    public Response deleteDevoir(@PathParam("id") int id){
        Devoir dev = devoirService.getById(id);
        devoirService.delete(dev);
        return Response.ok()
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "GET, PUT, OPTIONS, Content-Type")
                .build();
    }

    private void addIfDoesNotExist(Devoir devoir) {
        if(devoirService.findDevoir(devoir) == null) {
            devoirService.save(devoir);
        }
    }

}
