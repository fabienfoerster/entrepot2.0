package com.persistent.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.persistent.entity.User;
import com.persistent.service.UserService;

@Path("/user")
@Component
@Scope("request")
public class RestUser {
	
    @Autowired
    UserService userService;

    // The Java method will process HTTP GET requests
    @GET
    // The Java method will produce content identified by the MIME Media
    // type "text/plain"
    @Produces("text/plain")
    public Response getIt() {
        User user = new User();
        user.setLogin("casanova");

        addIfDoesNotExist(user);

        StringBuffer buffer = new StringBuffer();

        List<User> users = userService.getAll();
        for (User u : users) {
            buffer.append(u.getLogin()).append("\n");
        }

        String output = "Hi there (user): "+buffer.toString();
        return Response.ok()
                .entity(output)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Content-Type", "text/html; charset=UTF-8")
                .build();
    }
    private void addIfDoesNotExist(User u) {
        if(userService.findUser(u) == null) {
            userService.save(u);
        }
    }

}
