package com.persistent.service;

import java.util.List;

import com.persistent.entity.User;

public interface UserService {
	
	public boolean save(User user);
    public List<User> getAll();
    public User getByLogin(String s);
    public boolean delete(User u);
    public boolean update(User u);
    public User findUser(User u);

}
