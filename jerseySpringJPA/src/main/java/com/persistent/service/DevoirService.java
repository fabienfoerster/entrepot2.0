package com.persistent.service;

import com.persistent.entity.Devoir;

import java.util.List;

/**
 * Created by haitaar on 24/06/14.
 */
public interface DevoirService {
    public boolean save(Devoir devoir);
    public List<Devoir> getAll();
    public Devoir getById(int id);
    public boolean delete(Devoir devoir);
    public boolean update(Devoir devoir);
    public Devoir findDevoir(Devoir devoir);
    public List<Devoir> getByEns(String ens);
}
