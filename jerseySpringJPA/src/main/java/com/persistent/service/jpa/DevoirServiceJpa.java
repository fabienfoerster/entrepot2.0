package com.persistent.service.jpa;

import com.persistent.entity.Devoir;
import com.persistent.service.DevoirService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by haitaar on 24/06/14.
 */
@Service("devoirService")
public class DevoirServiceJpa implements DevoirService {

    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    @Transactional(readOnly=false, propagation= Propagation.REQUIRED)
    public boolean save(Devoir devoir) {
        entityManager.persist(devoir);
        entityManager.flush();

        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Devoir> getAll() {
        Query query = entityManager.createNamedQuery("Devoir.findAll");
        List<Devoir> devoirs = null;
        devoirs = query.getResultList();
        return devoirs;
    }

    @Override
    @Transactional(readOnly = true)
    public Devoir getById(int id) {
        return entityManager.find(Devoir.class, id);
    }

    @Override
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public boolean delete(Devoir devoir) {
        devoir = entityManager.getReference(Devoir.class, devoir.getId());
        if (devoir == null)
            return false;
        entityManager.remove(devoir);
        entityManager.flush();
        return true;
    }

    @Override
    @Transactional(readOnly=false, propagation=Propagation.REQUIRED)
    public boolean update(Devoir devoir) {
        entityManager.merge(devoir);
        entityManager.flush();
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public Devoir findDevoir(Devoir devoir) {
        Devoir result = null;
        Query queryFindPerson = entityManager.createNamedQuery("Devoir.findDevoir");
        queryFindPerson.setParameter("name", devoir.getName());
        queryFindPerson.setParameter("matiere", devoir.getMatiere());
        queryFindPerson.setParameter("date", devoir.getDate());
        queryFindPerson.setParameter("heure", devoir.getHeure());

        List<Devoir> devoirs = queryFindPerson.getResultList();
        if(devoirs.size() > 0) {
            result = devoirs.get(0);
        }
        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Devoir> getByEns(String ens){
        Query queryFindPerson = entityManager.createNamedQuery("Devoir.findDevoirEns");
        queryFindPerson.setParameter("enseignant", ens);

        List<Devoir> devoirs = queryFindPerson.getResultList();
        return devoirs;
    }
}
