package com.persistent.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by haitaar on 24/06/14.
 */
@Entity
@Table(name = "DEVOIR")
@NamedQueries( { @NamedQuery(name = "Devoir.findAll", query = "SELECT d FROM Devoir d"),
        @NamedQuery(name = "Devoir.findDevoir", query = "SELECT d FROM Devoir d " +
                "where d.name=:name and d.matiere=:matiere and d.date=:date and d.heure=:heure"),
        @NamedQuery(name = "Devoir.findDevoirEns", query = "SELECT d FROM Devoir d where d.enseignant=:enseignant")
})
public class Devoir {
    private int id;
    private String name;
    private String matiere;
    private String date;
    private String heure;
    private String description;
    private String enseignant;
    private String extensions;
    private String groupe;
    private String state;
    //private List<User> etudiants;

    @Id
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getMatiere() {
        return matiere;
    }
    public void setMatiere(String matiere) {
        this.matiere = matiere;
    }

    public String getHeure() {
        return heure;
    }
    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    /*@OneToMany
    public List<User> getEtudiants() {
        return etudiants;
    }
    public void setEtudiants(List<User> etudiants) {
        this.etudiants = etudiants;
    }*/

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getEnseignant() {
        return enseignant;
    }
    public void setEnseignant(String enseignant) {
        this.enseignant = enseignant;
    }

    public String getExtensions() {
        return extensions;
    }
    public void setExtensions(String extensions) {
        this.extensions = extensions;
    }

    public String getGroupe() {
        return groupe;
    }
    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(this.matiere).append(" : ")
                .append(this.name).append("\n");
        return builder.toString();
    }

}
