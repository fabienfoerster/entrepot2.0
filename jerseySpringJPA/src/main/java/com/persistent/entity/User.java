package com.persistent.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
@NamedQueries( { @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
        @NamedQuery(name = "User.findUser", query = "SELECT u FROM User u where u.login=:login")
})
public class User {
	
	private String login;

	@Id
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String toString(){
        StringBuilder res = new StringBuilder();
        res.append(" # USER > "+login);
        return res.toString();
    }

}
