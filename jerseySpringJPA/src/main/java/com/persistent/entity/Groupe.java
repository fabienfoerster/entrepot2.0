package com.persistent.entity;

public class Groupe {
	
	private String name;
	private String date;
	private String heure;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHeure() {
		return heure;
	}

	public void setHeure(String heure) {
		this.heure = heure;
	}
	
	
	
    public String toString(){
        StringBuilder res = new StringBuilder();
        res.append("- Groupe -\n");
        res.append("Nom : "+name+"\n");
        res.append("Date : "+date+"\n");
        res.append("Heure : "+heure);
        return res.toString();
    }
	
}
