package fr.unice.polytech.entrepot.git.api;

import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by foerster
 * on 20/06/14.
 */
public class Utils {

    public static String getJSON(String name,String password, String url) {
        try {

            String authString = name + ":" + password;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());

            String authStringEnc = new String(authEncBytes);
            URL u = new URL(url);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();

            c.setRequestMethod("GET");
            c.setRequestProperty("Authorization", "Basic " + authStringEnc);
            c.setRequestProperty("Content-length", "0");
            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line+"\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void sendJSON(String name,String password,String url,String body) throws IOException {

        String authString = name + ":" + password;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());

        String authStringEnc = new String(authEncBytes);
        URL u = new URL(url);
        HttpURLConnection c = (HttpURLConnection) u.openConnection();
        c.setDoOutput(true);
        c.setDoInput(true);

        c.setRequestMethod("POST");
        c.setRequestProperty("Authorization", "Basic " + authStringEnc);
        c.setRequestProperty("charset", "utf-8");
        c.setRequestProperty("Content-Type", "application/json");


        BufferedWriter out =
                new BufferedWriter(new OutputStreamWriter(c.getOutputStream()));
        out.write(body);
        out.close();


        //Get Response
        InputStream is = c.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuilder response = new StringBuilder();
        while((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\r');
        }

        c.disconnect();
    }

}
