package fr.unice.polytech.entrepot.git.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import fr.unice.polytech.entrepot.git.api.model.MyCommit;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.*;

/**
 * Created by foerster
 * on 12/06/14.
 */
@Path("/git")
public class GitRestApi {

    public static final String LOCALPATH = "/tmp/tilk/" ;
    public static final String RPCPATH = "http://54.76.238.132:8080/rpc?req=" ;
    public static final String HTTP = "http://" ;
    public static final String REMOTEPATH = "54.76.238.132:8080/r/";

    @Path("{repo_name}/log")
    @GET
    @Produces("application/json")
    public List<MyCommit> getLog(@PathParam("repo_name") String repoName,
                                 @QueryParam("login") String login,
                                 @QueryParam("password") String password) throws IOException, GitAPIException {
        String localPath = LOCALPATH + repoName  ;
        String remotePath =  HTTP + login + ":" +password + "@" + REMOTEPATH +repoName+".git";
        removeRepo(localPath);
        Git git = cloneRepo(localPath, remotePath);
        Iterable<RevCommit> commits = git.log().all().call();
        List<MyCommit> res = new ArrayList<MyCommit>();
        for(RevCommit commit : commits){
            res.add(new MyCommit(commit.getFullMessage(), new Date(Long.parseLong(commit.getCommitTime() + "000")),commit.getName()));
        }
        removeRepo(localPath);
        return res;
    }

    @GET
    @Produces("application/json")
    public String isAlive(){
        return "Oui je suis vivant";
    }


    @GET
    @Path("{repo_name}/create")
    public Response createRepo(@PathParam("repo_name") String repoName,
                               @QueryParam("login") String login,
                               @QueryParam("password") String password)  {
        JsonObject repoParams = new JsonObject();
        repoParams.addProperty("name", repoName);
        JsonArray owners = new JsonArray();
        owners.add(new JsonPrimitive("admin"));
        repoParams.add("owners",owners);
        repoParams.addProperty("accessRestriction","CLONE");
        repoParams.addProperty("authorizationControl","NAMED");
        try {
            Utils.sendJSON(login,password,RPCPATH+"CREATE_REPOSITORY",repoParams.toString());
        } catch (IOException e) {
            return Response.status(500)
                    .entity("Une erreur est survenue sur le serveur")
                    .build();
        }

        return Response.status(200)
                .entity(repoParams.toString())
                .build();
    }

    @GET
    @Path("{repo_name}/add/user/{username}")
    public Response addUser(@PathParam("repo_name") String repoName,
                            @PathParam("username") String username,
                            @QueryParam("login") String login,
                            @QueryParam("password") String password){
        JSONArray permissions = new JSONArray();
        JSONObject user = new JSONObject();
        user.put("registrant",username);
        user.put("permission","RW");
        user.put("registrantType","USER");
        user.put("permissionType","EXPLICIT");
        user.put("mutable",true);
        permissions.put(user);

        try {
            Utils.sendJSON(login,password,RPCPATH+"SET_REPOSITORY_MEMBER_PERMISSIONS&name="+repoName,permissions.toString());
        } catch (IOException e) {
            return Response.status(500)
                    .entity("Une erreur est survenue sur le serveur")
                    .build();
        }

        return Response.status(200)
                .entity(permissions.toString())
                .build();
    }

    @POST
    @Path("{repo_name}/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(FormDataMultiPart form,
                               @PathParam("repo_name") String repoName,
                               @QueryParam("login") String login,
                               @QueryParam("password") String password) throws GitAPIException, IOException {

        List<FormDataBodyPart> fileParts = form.getFields("file");

        String localPath = LOCALPATH + repoName  ;
        String remotePath =  HTTP + login + ":" +password + "@" + REMOTEPATH +repoName+".git";

        removeRepo(localPath);
        Git git = cloneRepo(localPath,remotePath);


        purgeDirectory(git, new File(localPath));

        for(FormDataBodyPart filePart : fileParts){
            InputStream fileInputStream = filePart.getValueAs(InputStream.class);
            ContentDisposition headerOfFilePart =  filePart.getContentDisposition();
            String filename = headerOfFilePart.getFileName();
            String uploadedFileLocation = localPath + "/"
                    + filename;

            //save it
            saveFile(fileInputStream, uploadedFileLocation);

            //add it
            git.add().addFilepattern(filename).call();
        }

        //commit it
        git.commit().setMessage(repoName).call();

        //push it
        git.push().call();

        removeRepo(localPath);

        String output = "Files uploaded to Hell !!!";

        return Response.status(200).entity(output).build();

    }


    @POST
    @Path("{devoir}/download")
    @Consumes("application/json")
    public Response downloadRendu(@PathParam("devoir") String devoir,
                                  List<String> listRendus,
                                  @QueryParam("login") String login,
                                  @QueryParam("password") String password) throws ZipException, GitAPIException, IOException {
        String zipArchive = LOCALPATH + devoir + ".zip" ;
        File zipArchiveFile = new File(zipArchive);
        if(zipArchiveFile.exists()){ zipArchiveFile.delete();}
        ZipFile zipFile = new ZipFile(zipArchive);
        // Initiate Zip Parameters which define various properties such
        // as compression method, etc.
        ZipParameters parameters = new ZipParameters();

        // set compression method to store compression
        parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

        // Set the compression level
        parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

        parameters.setReadHiddenFiles(false);
        for(String repoName : listRendus){
            String localPath = LOCALPATH + repoName  ;
            String remotePath =  HTTP + login + ":" +password + "@" + REMOTEPATH +repoName+".git";
            removeRepo(localPath);
            cloneRepo(localPath,remotePath);
            zipFile.addFolder(localPath,parameters);
            removeRepo(localPath);
        }

        File archive = new File(zipArchive);
        Response.ResponseBuilder response = Response.ok((Object) archive);
        response.header("Content-Disposition",
                "attachment; filename="+devoir+".zip");
        return response.build();
    }


    @GET
    @Path("{repo_name}/download/{commit_name}")
    public Response downloadFile(@PathParam("repo_name")String repoName,
                                 @PathParam("commit_name") String commitName,
                                 @QueryParam("login") String login,
                                 @QueryParam("password") String password) throws GitAPIException, ZipException, IOException {

        String localPath = LOCALPATH + repoName  ;
        String zipArchive = localPath + ".zip" ;
        String remotePath =  HTTP + login + ":" +password + "@" + REMOTEPATH +repoName+".git";
        removeRepo(localPath);
        Git git = cloneRepo(localPath,remotePath);
        git.checkout().setName(commitName).call();

        File zipArchiveFile = new File(zipArchive);
        if(zipArchiveFile.exists()){ zipArchiveFile.delete();}

        ZipFile zipFile = new ZipFile(zipArchive);
        // Initiate Zip Parameters which define various properties such
        // as compression method, etc.
        ZipParameters parameters = new ZipParameters();

        // set compression method to store compression
        parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

        // Set the compression level
        parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
        parameters.setReadHiddenFiles(false);

        zipFile.addFolder(localPath,parameters);


        removeRepo(localPath);

        File archive = new File(zipArchive);
        Response.ResponseBuilder response = Response.ok((Object) archive);
        response.header("Content-Disposition",
                "attachment; filename="+repoName+".zip");
        return response.build();
    }

    public Git cloneRepo(String localPath, String remotePath) throws GitAPIException {
        return Git.cloneRepository().setURI(remotePath)
                .setDirectory(new File(localPath)).call();
    }

    public void removeRepo(String localPath) throws IOException {
        File dir = new File(localPath);
        if(dir.exists()) {
            FileUtils.deleteDirectory(dir);
        }
    }

    private void purgeDirectory(Git git, File dir) throws GitAPIException {
        for(File file: dir.listFiles()) git.rm().addFilepattern(file.getName()).call();
    }

    // save uploaded file to a defined location on the server
    private void saveFile(InputStream uploadedInputStream, String serverLocation) {

        try {
            OutputStream outpuStream ;
            int read ;
            byte[] bytes = new byte[1024];

            outpuStream = new FileOutputStream(new File(serverLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                outpuStream.write(bytes, 0, read);
            }

            outpuStream.flush();
            outpuStream.close();

            uploadedInputStream.close();
        } catch (IOException e) {

            e.printStackTrace();
        }

    }


}
