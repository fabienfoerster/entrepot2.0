package fr.unice.polytech.entrepot.git.api.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by foerster
 * on 24/06/14.
 */
public class MyCommit {
    public String name ;
    public String date ;
    public String commitRef ;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public MyCommit(){}

    public MyCommit(String name, Date date, String commitRef) {
        this.name = name;
        this.date = dateFormat.format(date);
        this.commitRef = commitRef;
    }
}
